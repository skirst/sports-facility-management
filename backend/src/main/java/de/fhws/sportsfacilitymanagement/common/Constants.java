package de.fhws.sportsfacilitymanagement.common;

public class Constants {

    public static final String BASE_PATH = "/api/v1";
    public static final String LOGIN_PATH = BASE_PATH + "/login";
    public static final String REGISTER_PATH = BASE_PATH + "/auth/register";
    public static final String TOKEN_REFRESH_PATH = BASE_PATH + "/auth/token-refresh";
    public static final String REFRESH_TOKEN_HEADER = "RefreshToken";
    public static final int AUTH_TOKEN_EXPIRATION = 5;
    public static final int REFRESH_TOKEN_EXPIRATION = 30;

}
