package de.fhws.sportsfacilitymanagement.controller;

import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.dto.AddressDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SportsFacilityDto;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservationStatus;
import de.fhws.sportsfacilitymanagement.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/addresses")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:8080")
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public ResponseEntity<List<AddressDto>> findAllAddresses() {
        return addressService.findAllAddresses();
    }

    @GetMapping("/{addrId}")
    public ResponseEntity<AddressDto> findSingleAddress(@PathVariable Long addrId) {
        return addressService.findSingleAddress(addrId);
    }

    @PostMapping
    public ResponseEntity<AddressDto> createAddress(@RequestBody AddressDto addressDto) {
        return addressService.createAddress(addressDto);
    }

    @PutMapping("/{addrId}")
    public ResponseEntity<AddressDto> updateAddress(@PathVariable Long addrId, @RequestBody AddressDto newAddressDto) {
        return addressService.updateAddress(addrId, newAddressDto);
    }

    @DeleteMapping("/{addrId}")
    public void deleteAddress(@PathVariable Long addrId) {
        addressService.deleteAddress(addrId);
    }

    // ###### SPORTS FACILITIES ######

    @GetMapping("/{addrId}/sportsfacilities")
    public ResponseEntity<List<SportsFacilityDto>> findAllSportsFacilitiesOfASingleAddress(@PathVariable Long addrId) {
        return addressService.findAllSportsFacilitiesOfASingleAddress(addrId);
    }

    @GetMapping("/{addrId}/sportsfacilities/{facId}")
    public ResponseEntity<SportsFacilityDto> findSingleSportsFacilityOfASingleAddress(@PathVariable("addrId") Long addrId, @PathVariable("facId") Long facId) {
        return addressService.findSingleSportsFacilityOfASingleAddress(addrId, facId);
    }

    @PostMapping(value = "/{addrId}/sportsfacilities")
    public ResponseEntity<SportsFacilityDto> createSportsFacilityOfAddress(@PathVariable("addrId") Long addrId, @RequestBody SportsFacilityDto newFacility) {
        return addressService.createSportsFacilityOfAddress(addrId, newFacility);
    }

    @PutMapping(value = "/{addrId}/sportsfacilities/{facId}")
    public ResponseEntity<SportsFacilityDto> updateSportsFacilityOfAddress(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @RequestBody SportsFacilityDto newFacility) {
        return addressService.updateSportsFacilityOfAddress(addrId, facId, newFacility);
    }

    @DeleteMapping(value = "/{addrId}/sportsfacilities/{facId}")
    public void deleteSportsFacilityOfAddress(@PathVariable("addrId") Long addrId, @PathVariable("facId") Long facId) {
        addressService.deleteSportsFacilityOfAddress(addrId, facId);
    }

    // ###### RESERVATIONS ######

    @GetMapping("/{addrId}/sportsfacilities/{facId}/calendar")
    public ResponseEntity<List<Reservation>> getCalendarView(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId) {
        return addressService.getCalendarView(addrId, facId);
    }

    @GetMapping("/{addrId}/sportsfacilities/{facId}/reservations")
    public ResponseEntity<List<Reservation>> findAllReservationsOfSingleSportsFacilityOfSingleAddress(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId) {
        return addressService.findAllReservationsOfSingleSportsFacilityOfSingleAddress(addrId, facId);
    }

    @GetMapping("/{addrId}/sportsfacilities/{facId}/reservations/{reservId}")
    public ResponseEntity<Reservation> findSingleReservationOfSingleSportsFacilityOfSingleAddress(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @PathVariable("reservId") Long reservId) {
        return addressService.findSingleReservationOfSingleSportsFacilityOfSingleAddress(addrId, facId, reservId);
    }

    @PostMapping("/{addrId}/sportsfacilities/{facId}/reservations")
    public ResponseEntity<Reservation> createSingleReservationOfSingleSportsFacility(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @RequestBody Reservation reservation) {
        return addressService.createSingleReservationOfSingleSportsFacility(addrId, facId, reservation);
    }

    @PutMapping("/{addrId}/sportsfacilities/{facId}/reservations/{reservId}")
    public ResponseEntity<Reservation> updateSingleReservationOfSingleSportsFacility(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @PathVariable("reservId") Long reservId,
            @RequestBody Reservation reservation) {
        return addressService.updateSingleReservationOfSingleSportsFacility(addrId, facId, reservId, reservation);
    }

    @PutMapping("/{addrId}/sportsfacilities/{facId}/reservations/{reservId}/status")
    public ResponseEntity<Reservation> updateReservationStatus(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @PathVariable("reservId") Long reservId,
            @RequestBody ReservationStatus status) {
        return addressService.updateReservationStatus(addrId, facId, reservId, status);
    }

    @DeleteMapping("/{addrId}/sportsfacilities/{facId}/reservations/{reservId}")
    public void removeSingleReservationOfSingleSportsFacility(
            @PathVariable("addrId") Long addrId,
            @PathVariable("facId") Long facId,
            @PathVariable("reservId") Long reservId) {
        addressService.deleteSingleReservationOfSportsFacilityOfAddress(addrId, facId, reservId);
    }

}
