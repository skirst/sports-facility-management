package de.fhws.sportsfacilitymanagement.controller;

import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.dto.UserReadDto;
import de.fhws.sportsfacilitymanagement.domain.dto.UserWriteDto;
import de.fhws.sportsfacilitymanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<UserReadDto> findAllUsers() {
        return userService.findAll().stream().map(UserReadDto::from).toList();
    }

    @GetMapping("/{id}")
    public UserReadDto findSingleUser(@PathVariable Long id) {
        return userService.findById(id).map(UserReadDto::from)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<UserReadDto> createUser(@RequestBody UserWriteDto userWriteDto) {
        return ResponseEntity.ok(
                UserReadDto.from(userService.save(User.from(userWriteDto)))
        );
    }

    @PutMapping("/{id}")
    public UserReadDto updateUser(@PathVariable Long id, @RequestBody UserWriteDto newUserWriteDto) {
        return UserReadDto.from(
                userService.update(id, User.from(newUserWriteDto))
        );
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteById(id);
    }

}
