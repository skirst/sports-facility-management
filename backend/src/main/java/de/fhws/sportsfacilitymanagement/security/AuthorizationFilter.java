package de.fhws.sportsfacilitymanagement.security;


import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import de.fhws.sportsfacilitymanagement.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.fhws.sportsfacilitymanagement.common.Constants.LOGIN_PATH;
import static de.fhws.sportsfacilitymanagement.common.Constants.TOKEN_REFRESH_PATH;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
@RequiredArgsConstructor
public class AuthorizationFilter extends OncePerRequestFilter {

    private final JwtUtil jwtUtil;
    private final AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (request.getServletPath().equals(LOGIN_PATH) || request.getServletPath().equals(TOKEN_REFRESH_PATH)) {
            filterChain.doFilter(request, response);
        } else {
            String authorizationHeader = request.getHeader(AUTHORIZATION);
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                    String token = authorizationHeader.substring("Bearer ".length());
                    DecodedJWT decodedToken;
                    try {
                         decodedToken = jwtUtil.getDecodedToken(token, request);
                    } catch (JWTVerificationException e) {
                        //responds with 401 instead of 500 so frontend can use this info for refresh attempt
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Authorization token isn't valid! " + e.getMessage());
                        return;
                    }
                    // read roles from DB instead of JWT due to security risks
                    var user = authService.loadUserByUsername(decodedToken.getSubject());
                    var authToken = jwtUtil.getUsernamePasswordAuthenticationToken(decodedToken, user.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                    filterChain.doFilter(request, response);
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }



}
