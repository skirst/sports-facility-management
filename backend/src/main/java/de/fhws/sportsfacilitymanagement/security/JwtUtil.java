package de.fhws.sportsfacilitymanagement.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;

@Component
public class JwtUtil {

    @Value("${jwt_secret}")
    private String secret;

    public String generateToken(final HttpServletRequest request, final String username, final Collection<? extends GrantedAuthority> authorities, final long expiresInMinutes) throws IllegalArgumentException, JWTCreationException {
        Algorithm algorithm = Algorithm.HMAC256(secret.getBytes(StandardCharsets.UTF_8));

        return JWT.create()
                .withSubject(username)
                .withClaim("roles", authorities.stream().map(GrantedAuthority::getAuthority).toList())
                .withExpiresAt(new Date(System.currentTimeMillis() + expiresInMinutes * 60 * 1000))
                .withIssuedAt(new Date())
                // issuer = application/project/company name OR URL
                .withIssuer(getBaseUrl(request))
                .sign(algorithm);
    }

    public DecodedJWT getDecodedToken(final String token, final HttpServletRequest request) throws JWTVerificationException {
        Algorithm algorithm = Algorithm.HMAC256(secret.getBytes(StandardCharsets.UTF_8));

        JWTVerifier verifier = JWT.require(algorithm)
                .withClaimPresence("sub")
                .withClaimPresence("roles")
                .withClaimPresence("exp")
                .withClaimPresence("iat")
                .withIssuer(getBaseUrl(request))
                .acceptExpiresAt(0)
                .build();
        return verifier.verify(token);
    }

    public UsernamePasswordAuthenticationToken getUsernamePasswordAuthenticationToken(final DecodedJWT decodedJWT, final Collection<? extends GrantedAuthority> authorities) {
            var extendedAuths = authorities.stream()
                .map(auth -> "ROLE_".concat(auth.getAuthority()))
                .map(SimpleGrantedAuthority::new)
                .toList();
        return new UsernamePasswordAuthenticationToken(decodedJWT.getSubject(), null, extendedAuths);
    }

    private static String getBaseUrl(HttpServletRequest request) {
        return ServletUriComponentsBuilder.fromRequestUri(request)
                .replacePath(null)
                .build()
                .toUriString();
    }

}
