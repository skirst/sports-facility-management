package de.fhws.sportsfacilitymanagement.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static de.fhws.sportsfacilitymanagement.common.Constants.*;
import static org.springframework.http.HttpHeaders.*;

@RequiredArgsConstructor
@Slf4j
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        return authenticationManager.authenticate(authenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) {
        String accessToken = jwtUtil.generateToken(request, authentication.getName(), authentication.getAuthorities(), AUTH_TOKEN_EXPIRATION);
        String refreshToken = jwtUtil.generateToken(request, authentication.getName(), authentication.getAuthorities(), REFRESH_TOKEN_EXPIRATION);
        response.setHeader(AUTHORIZATION, accessToken);
        response.setHeader(REFRESH_TOKEN_HEADER, refreshToken);
    }

}
