package de.fhws.sportsfacilitymanagement.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.*;
import de.fhws.sportsfacilitymanagement.domain.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SubResourceSerialization {
    public interface Identifiable {
        Long getId();
    }
    public static class IdExtractSerializer<T extends Identifiable> extends JsonSerializer<Collection<T>> {
        @Override
        public void serialize(Collection<T> value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            long[] ids = value.stream().mapToLong(Identifiable::getId).toArray();
            jsonGenerator.writeArray(ids, 0, ids.length);
        }
    }
    public static class EmptyCollectionDeserializer<T extends Identifiable> extends JsonDeserializer<Collection<T>> {
        @Override
        public Collection<T> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            return new ArrayList<>();
        }
    }

    public static class SupervisorsDtoSerializer extends JsonSerializer<Collection<User>> {
        @Override
        public void serialize(Collection<User> value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeStartArray();
            for (User u : value) {
                SubResourceSerialization.serializeUserDto(u, jsonGenerator, true);
            }
            jsonGenerator.writeEndArray();
        }
    }

    public static class SupervisorsDtoDeserializer extends JsonDeserializer<Collection<User>> {
        @Override
        public Collection<User> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            final ObjectCodec objectCodec = jsonParser.getCodec();
            final JsonNode listNode = objectCodec.readTree(jsonParser);
            final List<User> result = new ArrayList<>();
            for (JsonNode node : listNode) {
                var user = new User();
                user.setId(node.asLong());
                result.add(user);
            }
            return result;
        }
    }

    public static class ApplicantDtoSerializer extends JsonSerializer<User> {
        @Override
        public void serialize(User value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            SubResourceSerialization.serializeUserDto(value, jsonGenerator, false);
        }
    }

    private static void serializeUserDto(User user, JsonGenerator jsonGenerator, Boolean useTelephoneNumber) throws IOException {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", user.getId());
            jsonGenerator.writeStringField("username", user.getUsername());
            if (useTelephoneNumber) {
                jsonGenerator.writeStringField("telephoneNumber", user.getTelephoneNumber());
            }
            jsonGenerator.writeEndObject();
    }

}


