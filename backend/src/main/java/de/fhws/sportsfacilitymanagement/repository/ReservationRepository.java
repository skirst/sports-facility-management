package de.fhws.sportsfacilitymanagement.repository;

import de.fhws.sportsfacilitymanagement.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM sports_db.reservation WHERE (user_id = :userId);", nativeQuery = true)
    int deleteAllReservationsOfUser(@Param("userId") Long userId);

    @Query(
            value = "SELECT * FROM sports_db.reservation " +
                    "WHERE " + // case: new start time is within existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time <= :startTime " +       // new start is after/equal to exist
                    "    AND " +
                    "    end_time > :startTime " +          // new start is before end of exist
                    ") " +
                    "OR " + // case: new end time is within existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time <= :endTime " +             // new end is after/equal to exist start
                    "    AND " +
                    "    end_time > :endTime " +                // new end is within/before exist end
                    ") " +
                    "OR " + // case: new start time is before and new end time is after existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time >= :startTime " +           // new start is before exist start
                    "    AND " +
                    "    end_time <= :endTime " +               // new end is after exist end
                    ");",
            nativeQuery = true
    )
    List<Reservation> findAllTimeCollisions(@Param("sportsFacId") Long sportsFacId, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);

    @Query(
            value = "SELECT * FROM sports_db.reservation " +
                    "WHERE " + // case: new start time is within existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time <= :startTime " +       // new start is after/equal to exist
                    "    AND " +
                    "    end_time > :startTime " +          // new start is before end of exist
                    ");",
            nativeQuery = true
    )
    List<Reservation> findStartTimeCollisions(@Param("sportsFacId") Long sportsFacId, @Param("startTime") LocalDateTime startTime);

    @Query(
            value = "SELECT * FROM sports_db.reservation " +
                    "WHERE " + // case: new end time is within existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time <= :endTime " +             // new end is after/equal to exist start
                    "    AND " +
                    "    end_time > :endTime " +                // new end is within/before exist end
                    ");",
            nativeQuery = true
    )
    List<Reservation> findEndTimeCollisions(@Param("sportsFacId") Long sportsFacId, @Param("endTime") LocalDateTime endTime);

    @Query(
            value = "SELECT * FROM sports_db.reservation " +
                    "WHERE " + // case: new start time is before and new end time is after existing reservation times
                    "(" +
                    "    sportsfacility_id = :sportsFacId " +
                    "    AND " +
                    "    start_time >= :startTime " +           // new start is before exist start
                    "    AND " +
                    "    end_time <= :endTime " +               // new end is after exist end
                    ");",
            nativeQuery = true
    )
    List<Reservation> findOverlappingCollisions(@Param("sportsFacId") Long sportsFacId, @Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);


}
