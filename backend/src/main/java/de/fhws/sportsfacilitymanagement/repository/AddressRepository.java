package de.fhws.sportsfacilitymanagement.repository;

import de.fhws.sportsfacilitymanagement.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
