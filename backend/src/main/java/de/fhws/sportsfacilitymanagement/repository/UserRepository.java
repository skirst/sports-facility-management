package de.fhws.sportsfacilitymanagement.repository;

import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(final String username);

    Long countByRole(final Role role);

}
