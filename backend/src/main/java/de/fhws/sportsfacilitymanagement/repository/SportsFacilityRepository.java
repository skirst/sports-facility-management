package de.fhws.sportsfacilitymanagement.repository;

import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SportsFacilityRepository extends JpaRepository<SportsFacility, Long> {
}
