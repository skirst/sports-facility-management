package de.fhws.sportsfacilitymanagement.domain.enums;

public enum CourtSurface {
    GRASS,
    ARTIFICIAL_TURF

}
