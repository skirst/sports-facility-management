package de.fhws.sportsfacilitymanagement.domain.dto;

import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.Identifiable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class UserWriteDto implements Serializable, Identifiable {

    private Long id;

    private String username;

    private String password;

    private String fullname;

    private String email;

    private String telephoneNumber;

    private Role role;

    public static UserWriteDto from(User user) {
        return new UserWriteDto(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getFullname(),
                user.getEmail(),
                user.getTelephoneNumber(),
                user.getRole()
        );
    }
}
