package de.fhws.sportsfacilitymanagement.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.fhws.sportsfacilitymanagement.domain.dto.SportsFacilityDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SupervisorDto;
import de.fhws.sportsfacilitymanagement.domain.enums.CourtSurface;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.Identifiable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "sports_facility", schema = "sports_db")
@Data
@NoArgsConstructor
public class SportsFacility implements Serializable, Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "has_floodlight", nullable = false)
    private boolean hasFloodlight;

    @Column(name = "court_surface", nullable = false)
    @Enumerated(EnumType.STRING)
    private CourtSurface courtSurface;

    // UNI-DIRECTIONAL
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "sportsfacility_id")
    @JsonSerialize(using = SubResourceSerialization.IdExtractSerializer.class)
    @JsonDeserialize(using = SubResourceSerialization.EmptyCollectionDeserializer.class)
    private List<Reservation> reservations;

    @ManyToMany(fetch = FetchType.LAZY,
        cascade = {CascadeType.MERGE})
    @JoinTable(name = "sports_facility_user",
        schema = "sports_db",
        joinColumns = @JoinColumn(name = "sports_fac_id"),
        inverseJoinColumns = @JoinColumn(name = "user_id"))
    @JsonSerialize(using = SubResourceSerialization.SupervisorsDtoSerializer.class)
    @JsonDeserialize(using = SubResourceSerialization.SupervisorsDtoDeserializer.class)
    private List<User> supervisors = new ArrayList<>();

    public static SportsFacility from(SportsFacilityDto facilityDto) {
        var facility = new SportsFacility();
        facility.setId(facilityDto.getId());
        facility.setName(facilityDto.getName());
        facility.setHasFloodlight(facilityDto.isHasFloodlight());
        facility.setCourtSurface(facilityDto.getCourtSurface());
        facility.setReservations(new ArrayList<>());
        facility.setSupervisors(facilityDto.getSupervisors().stream()
                .map(User::from).toList());
        return facility;
    }
}
