package de.fhws.sportsfacilitymanagement.domain.enums;

public enum ReservationStatus {
    PENDING,
    APPROVED,
    DENIED
}
