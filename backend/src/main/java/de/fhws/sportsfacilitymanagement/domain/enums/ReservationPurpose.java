package de.fhws.sportsfacilitymanagement.domain.enums;

public enum ReservationPurpose {
    GAME,
    TRAINING,
    RESERVED,
    BLOCKED,
    MISC
}
