package de.fhws.sportsfacilitymanagement.domain.enums;

public enum ReservedSpace {
    THIRD_OF_FIELD,
    HALF_OF_FIELD,
    FULL_FIELD
}
