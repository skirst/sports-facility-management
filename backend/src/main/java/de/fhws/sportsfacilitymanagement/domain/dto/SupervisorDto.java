package de.fhws.sportsfacilitymanagement.domain.dto;

import de.fhws.sportsfacilitymanagement.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class SupervisorDto {

    private Long id;

    private String username;

    private String telephoneNumber;

    public static SupervisorDto from(User user) {
        return new SupervisorDto(
                user.getId(),
                user.getUsername(),
                user.getTelephoneNumber()
                );
    }
}