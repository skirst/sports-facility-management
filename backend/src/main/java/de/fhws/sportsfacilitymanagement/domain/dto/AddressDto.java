package de.fhws.sportsfacilitymanagement.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.fhws.sportsfacilitymanagement.domain.Address;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class AddressDto {
    private Long id;

    private String street;

    private String houseNumber;

    private int postalCode;

    private String location;

    @JsonDeserialize(using = SubResourceSerialization.EmptyCollectionDeserializer.class)
    private List<Long> sportsFacilities;

    public static AddressDto from(Address address) {
        return new AddressDto(
                address.getId(),
                address.getStreet(),
                address.getHouseNumber(),
                address.getPostalCode(),
                address.getLocation(),
                address.getSportsFacilities().stream().map(SportsFacility::getId).collect(Collectors.toList())
        );
    }

}
