package de.fhws.sportsfacilitymanagement.domain.dto;

import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.Identifiable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class UserReadDto implements Serializable, Identifiable {

    private Long id;

    private String username;

    private String fullname;

    private String email;

    private String telephoneNumber;

    private Role role;

    private List<Long> sportsFacilities;

    private List<Long> reservations;


    public static UserReadDto from(User user) {
        return new UserReadDto(
                user.getId(),
                user.getUsername(),
                user.getFullname(),
                user.getEmail(),
                user.getTelephoneNumber(),
                user.getRole(),
                user.getSportsFacilities().stream().map(SportsFacility::getId).toList(),
                user.getReservations().stream().map(Reservation::getId).toList()
        );
    }

}
