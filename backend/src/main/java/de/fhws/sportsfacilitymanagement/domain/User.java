package de.fhws.sportsfacilitymanagement.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.fhws.sportsfacilitymanagement.domain.dto.ApplicantDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SupervisorDto;
import de.fhws.sportsfacilitymanagement.domain.dto.UserWriteDto;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.Identifiable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user", schema = "sports_db")
@Data
@NoArgsConstructor
public class User implements Serializable, Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    // to avoid inclusion of password in JSON response
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ToString.Exclude
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "fullname", nullable = false)
    private String fullname;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "telephone_number")
    private String telephoneNumber;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @ManyToMany(
            mappedBy = "supervisors",
            cascade = {
                    CascadeType.MERGE
            })
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<SportsFacility> sportsFacilities = new HashSet<>();

    // UNI-DIRECTIONAL
    @OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JoinColumn(name = "user_id")
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Reservation> reservations = new HashSet<>();


    public static User from(UserWriteDto userWriteDto) {
        var user = new User();
        user.setId(userWriteDto.getId());
        user.setUsername(userWriteDto.getUsername());
        user.setPassword(userWriteDto.getPassword());
        user.setFullname(userWriteDto.getFullname());
        user.setEmail(userWriteDto.getEmail());
        user.setTelephoneNumber(userWriteDto.getTelephoneNumber());
        user.setRole(userWriteDto.getRole());
        return user;
    }

    public static User from(ApplicantDto applicantDto) {
        var user = new User();
        user.setId(applicantDto.getId());
        user.setUsername(applicantDto.getUsername());
        return user;
    }

    public static User from(SupervisorDto supervisorDto) {
        var user = new User();
        user.setId(supervisorDto.getId());
        user.setUsername(supervisorDto.getUsername());
        user.setTelephoneNumber(supervisorDto.getTelephoneNumber());
        return user;
    }
}
