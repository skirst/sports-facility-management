package de.fhws.sportsfacilitymanagement.domain.enums;

public enum Role {
    ADMIN,
    CLUB_SUPERVISOR,
    TRAINER,
    PLAYER
}
