package de.fhws.sportsfacilitymanagement.domain.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.CourtSurface;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
@Setter
public class SportsFacilityDto implements SubResourceSerialization.Identifiable {

    private Long id;

    private String name;

    private boolean hasFloodlight;

    private CourtSurface courtSurface;

    @JsonDeserialize(using = SubResourceSerialization.EmptyCollectionDeserializer.class)
    private List<Long> reservations;

    private List<SupervisorDto> supervisors;

    public static SportsFacilityDto from(SportsFacility facility) {
        return new SportsFacilityDto(
                facility.getId(),
                facility.getName(),
                facility.isHasFloodlight(),
                facility.getCourtSurface(),
                facility.getReservations().stream().map(Reservation::getId).collect(Collectors.toList()),
                facility.getSupervisors().stream().map(SupervisorDto::from).collect(Collectors.toList())
        );
    }
}

