package de.fhws.sportsfacilitymanagement.domain.dto;

import de.fhws.sportsfacilitymanagement.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class ApplicantDto {

    private Long id;

    private String username;

    public static ApplicantDto from(User user) {
        return new ApplicantDto(
                user.getId(),
                user.getUsername()
        );
    }
}
