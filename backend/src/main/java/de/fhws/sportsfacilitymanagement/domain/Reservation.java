package de.fhws.sportsfacilitymanagement.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservationPurpose;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservationStatus;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservedSpace;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.Identifiable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "reservation", schema = "sports_db")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reservation implements Serializable, Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonSerialize(using = SubResourceSerialization.ApplicantDtoSerializer.class)
    private User applicant;

    @Column(name = "reservation_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private ReservationStatus reservationStatus;

    @Column(name = "reservation_purpose", nullable = false)
    @Enumerated(EnumType.STRING)
    private ReservationPurpose reservationPurpose;

    @Column(name = "reserved_space", nullable = false)
    @Enumerated(EnumType.STRING)
    private ReservedSpace reservedSpace;

}
