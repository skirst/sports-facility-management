package de.fhws.sportsfacilitymanagement.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.fhws.sportsfacilitymanagement.domain.dto.AddressDto;
import de.fhws.sportsfacilitymanagement.serialization.SubResourceSerialization.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "address", schema = "sports_db")
@Data
@NoArgsConstructor
public class Address implements Serializable, Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "street", nullable = false)
    private String street;

    // uses String to cover cases like "11a"
    @Column(name = "house_number", nullable = false)
    private String houseNumber;

    @Column(name = "postal_code", nullable = false)
    private int postalCode;

    @Column(name = "location", nullable = false)
    private String location;

    // UNI-DIRECTIONAL
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "address_id")
    @JsonSerialize(using = IdExtractSerializer.class)
    @JsonDeserialize(using = EmptyCollectionDeserializer.class)
    private List<SportsFacility> sportsFacilities;

    public static Address from(AddressDto addressDto) {
        var address = new Address();
        address.setStreet(addressDto.getStreet());
        address.setHouseNumber(addressDto.getHouseNumber());
        address.setPostalCode(addressDto.getPostalCode());
        address.setLocation(addressDto.getLocation());
        address.setSportsFacilities(new ArrayList<>());
        return address;
    }
}
