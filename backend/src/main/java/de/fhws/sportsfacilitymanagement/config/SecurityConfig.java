package de.fhws.sportsfacilitymanagement.config;

import de.fhws.sportsfacilitymanagement.security.AuthenticationFilter;
import de.fhws.sportsfacilitymanagement.security.AuthorizationFilter;
import de.fhws.sportsfacilitymanagement.security.JwtUtil;
import de.fhws.sportsfacilitymanagement.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static de.fhws.sportsfacilitymanagement.common.Constants.REFRESH_TOKEN_HEADER;
import static de.fhws.sportsfacilitymanagement.domain.enums.Role.*;
import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthService authService;
    private final JwtUtil jwtUtil;

    private HttpSecurity configureRoutesSecurity(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests(auth -> auth
                                // !! more specific rules have to go first - otherwise the common rule is matched first + rest ignored
                                // ADMIN needs to be manually included in these specific routes if he's supposed to be able to access all routes
                                .mvcMatchers(GET, "/auth/token-refresh").permitAll()
                                .mvcMatchers(POST, "/addresses/{addrId}/sportsfacilities/{facId}/reservations").hasAnyRole(ADMIN.name(), TRAINER.name())
                                .mvcMatchers(PUT, "/addresses/{addrId}/sportsfacilities/{facId}/reservations/{reservId}").hasAnyRole(ADMIN.name(), CLUB_SUPERVISOR.name())
                                .mvcMatchers(PUT, "/addresses/{addrId}/sportsfacilities/{facId}/reservations/{reservId}/status").hasAnyRole(ADMIN.name(), CLUB_SUPERVISOR.name())
                                .mvcMatchers(DELETE, "/addresses/{addrId}/sportsfacilities/{facId}/reservations/{reservId}").hasAnyRole(ADMIN.name(), TRAINER.name())
                                .mvcMatchers(GET, "/addresses/**", "/auth/info").hasAnyRole(ADMIN.name(), CLUB_SUPERVISOR.name(), TRAINER.name(), PLAYER.name())
                                // Admin may access any (remaining!) routes, but has to be included explicitly for previous explicitly named routes to get access to these
                                .mvcMatchers("/**").hasRole(ADMIN.name())
                                // To prevent non-configured routes from being accessed -> forces dev to implement specific security
                                .anyRequest().denyAll()
                );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var authFilter = new AuthenticationFilter(authenticationManagerBean(), jwtUtil);
        authFilter.setFilterProcessesUrl("/login");

        final UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin( "http://localhost:8080" );
        corsConfiguration.setAllowedMethods(Stream.of(GET, POST, PUT, DELETE, OPTIONS).map(Enum::name).collect(Collectors.toList()));
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setMaxAge(Duration.ofSeconds(3600));
        corsConfiguration.setExposedHeaders(List.of(HttpHeaders.AUTHORIZATION, REFRESH_TOKEN_HEADER));
        corsConfiguration.applyPermitDefaultValues();
        corsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);

        configureRoutesSecurity(http)
                .csrf().disable()
                .httpBasic().disable()
                .cors()
                .configurationSource(corsConfigurationSource)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(new AuthorizationFilter(jwtUtil, authService), UsernamePasswordAuthenticationFilter.class)
                .addFilter(authFilter);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        // defaults to BCrypt
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
