package de.fhws.sportsfacilitymanagement.service;

import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ReservationRepository reservationRepository;

    @PreAuthorize("hasRole('ADMIN')")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Optional<User> findById(final Long id) {
        return userRepository.findById(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User findByUsername(final String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Found no user with name " + username + "."));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User save(User user) {
        if (user.getPassword().length() < 8) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The password has to contain at least 8 characters!");
        }
        log.info("Saving new user {} to the database", user.getUsername());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public User update(final Long id, User newUser) {
        if (newUser.getPassword().length() < 8) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The password has to contain at least 8 characters!");
        }
        if (checkIfUserIsLastAdmin(id) && newUser.getRole() != Role.ADMIN) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot change the role of the last remaining admin!");
        }
        if (checkIfUserIsLastSupervisorOfAFacility(id)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot change the role of the last remaining supervisor assigned to a facility!");
        }
        return userRepository.findById(id)
                .map(user -> {
                    user.setUsername(newUser.getUsername());
                    user.setPassword(passwordEncoder.encode(newUser.getPassword()));
                    user.setFullname(newUser.getFullname());
                    user.setEmail(newUser.getEmail());
                    user.setTelephoneNumber(newUser.getTelephoneNumber());
                    user.setRole(newUser.getRole());
                    user.setSportsFacilities(newUser.getSportsFacilities());
                    return userRepository.save(user);
                })
                .orElseThrow(() -> new UsernameNotFoundException("Found no user with id " + id + "."));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public void deleteById(final Long id) {
        if (checkIfUserIsLastAdmin(id)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot delete the last admin!");
        }
        if (checkIfUserIsSupervisorOfAFacility(id)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot delete supervisor as long as he is assigned to a facility!");
        }

        reservationRepository.deleteAllReservationsOfUser(id);
        userRepository.deleteById(id);
    }

    private boolean checkIfUserIsLastAdmin(final Long id) {
        var user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User doesn't exist."));

        if (user.getRole() == Role.ADMIN) {
            var adminCount = userRepository.countByRole(Role.ADMIN);
            return adminCount < 2;
        }
        return false;
    }

    private boolean checkIfUserIsSupervisorOfAFacility(final Long id) {
        var user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User doesn't exist."));

        if (user.getRole() == Role.CLUB_SUPERVISOR) {
            return user.getSportsFacilities().size() > 0;
        }
        return false;
    }

    private boolean checkIfUserIsLastSupervisorOfAFacility(final Long id) {
        var user = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User doesn't exist."));
        if (user.getRole() == Role.CLUB_SUPERVISOR) {
            for (SportsFacility fac : user.getSportsFacilities()) {
                if (fac.getSupervisors().size() == 1) {
                    //user is the only supervisor of facility
                    return true;
                }
            }
        }
        return false;
    }

}
