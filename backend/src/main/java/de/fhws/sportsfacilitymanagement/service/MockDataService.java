package de.fhws.sportsfacilitymanagement.service;

import de.fhws.sportsfacilitymanagement.domain.Address;
import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.enums.*;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class MockDataService {

    private final AddressRepository addressRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    void run() {
        System.out.println("TEST main RUNNING");
        User thomas = new User();
        thomas.setUsername("tommy");
        thomas.setPassword(passwordEncoder.encode("a_secret_pw"));
        thomas.setFullname("Thomas Schmidt");
        thomas.setEmail("thomas-mail@hoster.com");
        thomas.setTelephoneNumber("123456789");
        thomas.setRole(Role.ADMIN);
        userRepository.save(thomas);

        User sandra = new User();
        sandra.setUsername("sanny");
        sandra.setPassword(passwordEncoder.encode("another_secret_pw"));
        sandra.setFullname("Sandra Müller");
        sandra.setEmail("sandra-mail@hoster.com");
        sandra.setTelephoneNumber("135792468");
        sandra.setRole(Role.PLAYER);
        userRepository.save(sandra);

        User karl = new User();
        karl.setUsername("kalli");
        karl.setPassword(passwordEncoder.encode("my-password"));
        karl.setFullname("Karl Huber");
        karl.setEmail("sportsfacility@protonmail.com");
        karl.setTelephoneNumber("135792454668");
        karl.setRole(Role.TRAINER);
        userRepository.save(karl);

        User richard = new User();
        richard.setUsername("richie");
        richard.setPassword(passwordEncoder.encode("my-pw"));
        richard.setFullname("Richard Fischer");
        richard.setEmail("rich-mail@hoster.com");
        richard.setTelephoneNumber("195670836068");
        richard.setRole(Role.CLUB_SUPERVISOR);
        userRepository.save(richard);
        //oldMockData(thomas, sandra, karl, richard);


        var sup1 = buildSupervisor("Supervisor1");
        userRepository.save(sup1);
        var sup2 = buildSupervisor("Supervisor2");
        userRepository.save(sup2);
        var sup3 = buildSupervisor("Supervisor3");
        userRepository.save(sup3);
        var sup4 = buildSupervisor("Supervisor4");
        userRepository.save(sup4);
        var sup5 = buildSupervisor("Supervisor5");
        userRepository.save(sup5);

        Address erlabrunn = new Address();
        erlabrunn.setStreet("Schleusenweg");
        erlabrunn.setHouseNumber("2");
        erlabrunn.setPostalCode(97250);
        erlabrunn.setLocation("Erlabrunn");
        erlabrunn.setSportsFacilities(new ArrayList<>());

        Address margetshöchheim = new Address();
        margetshöchheim.setStreet("Am Sportplatz");
        margetshöchheim.setHouseNumber("1a");
        margetshöchheim.setPostalCode(97276);
        margetshöchheim.setLocation("Margetshöchheim");
        margetshöchheim.setSportsFacilities(new ArrayList<>());

        Address zellAmMain = new Address();
        zellAmMain.setStreet("Scheckertstraße");
        zellAmMain.setHouseNumber("13");
        zellAmMain.setPostalCode(97299);
        zellAmMain.setLocation("Zell am Main");
        zellAmMain.setSportsFacilities(new ArrayList<>());

        SportsFacility rasenErlabrunn = new SportsFacility();
        rasenErlabrunn.setName("Rasen Erlabrunn");
        rasenErlabrunn.setHasFloodlight(false);
        rasenErlabrunn.setCourtSurface(CourtSurface.GRASS);
        rasenErlabrunn.setSupervisors(Arrays.asList(sup1));
        rasenErlabrunn.setReservations(new ArrayList<>());

        SportsFacility kunstrasenErlabrunn = new SportsFacility();
        kunstrasenErlabrunn.setName("Kunstrasen Erlabrunn");
        kunstrasenErlabrunn.setHasFloodlight(true);
        kunstrasenErlabrunn.setCourtSurface(CourtSurface.ARTIFICIAL_TURF);
        kunstrasenErlabrunn.setSupervisors(Arrays.asList(sup2));
        kunstrasenErlabrunn.setReservations(new ArrayList<>());

        SportsFacility rasenMargetshöchheim = new SportsFacility();
        rasenMargetshöchheim.setName("Rasen Margetshöchheim");
        rasenMargetshöchheim.setHasFloodlight(true);
        rasenMargetshöchheim.setCourtSurface(CourtSurface.GRASS);
        rasenMargetshöchheim.setSupervisors(Arrays.asList(sup3));
        rasenMargetshöchheim.setReservations(new ArrayList<>());

        SportsFacility untererRasenZellAmMain = new SportsFacility();
        untererRasenZellAmMain.setName("Unterer Rasen");
        untererRasenZellAmMain.setHasFloodlight(true);
        untererRasenZellAmMain.setCourtSurface(CourtSurface.GRASS);
        untererRasenZellAmMain.setSupervisors(Arrays.asList(sup4));
        untererRasenZellAmMain.setReservations(new ArrayList<>());

        SportsFacility obererRasenZellAmMain = new SportsFacility();
        obererRasenZellAmMain.setName("Oberer Rasen");
        obererRasenZellAmMain.setHasFloodlight(true);
        obererRasenZellAmMain.setCourtSurface(CourtSurface.GRASS);
        obererRasenZellAmMain.setSupervisors(Arrays.asList(sup5));
        obererRasenZellAmMain.setReservations(new ArrayList<>());

        Reservation res_r_erl_1 = buildNewReservation(thomas, "U15", 8, 10, 17, 30, 19, 0, ReservedSpace.HALF_OF_FIELD);
        rasenErlabrunn.setReservations(Arrays.asList(res_r_erl_1));

        Reservation res1 = buildNewReservation(thomas, "U19", 8, 8, 19, 0, 20, 30, ReservedSpace.FULL_FIELD);
        Reservation res2 = buildNewReservation(thomas, "U19", 8, 11, 19, 0, 20, 30, ReservedSpace.FULL_FIELD);
        Reservation res3 = buildNewReservation(thomas, "U15", 8, 8, 17, 30, 19, 0, ReservedSpace.FULL_FIELD);
        Reservation res4 = buildNewReservation(thomas, "U13-1", 8, 9, 17, 0, 19, 0, ReservedSpace.FULL_FIELD);
        Reservation res5 = buildNewReservation(thomas, "U13-1", 8, 11, 17, 0, 19, 0, ReservedSpace.FULL_FIELD);
        res5.setReservationPurpose(ReservationPurpose.GAME);
        Reservation res6 = buildNewReservation(thomas, "U13-2", 8, 10, 17, 30, 19, 0, ReservedSpace.HALF_OF_FIELD);
        Reservation res7 = buildNewReservation(thomas, "U11-2", 8, 8, 16, 30, 17, 30, ReservedSpace.FULL_FIELD);
        Reservation res8 = buildNewReservation(thomas, "U9-2", 8, 9, 16, 30, 18, 0, ReservedSpace.HALF_OF_FIELD);
        Reservation res9 = buildNewReservation(thomas, "U7 TSV", 8, 10, 16, 30, 17, 30, ReservedSpace.HALF_OF_FIELD);
        var reservations = Arrays.asList(res1, res2, res3, res4, res5, res6, res7, res8, res9);
        kunstrasenErlabrunn.setReservations(reservations);
        erlabrunn.setSportsFacilities(Arrays.asList(rasenErlabrunn, kunstrasenErlabrunn));
        addressRepository.save(erlabrunn);

        Reservation res_marg_1 = buildNewReservation(thomas, "U13-1", 8, 9, 17, 0, 19, 0, ReservedSpace.FULL_FIELD);
        Reservation res_marg_2 = buildNewReservation(thomas, "U11-2", 8, 10, 16, 15, 17, 30, ReservedSpace.FULL_FIELD);
        Reservation res_marg_3 = buildNewReservation(thomas, "U9-1", 8, 10, 17, 30, 18, 30, ReservedSpace.FULL_FIELD);
        res_marg_3.setReservationPurpose(ReservationPurpose.GAME);
        Reservation res_marg_4 = buildNewReservation(thomas, "U7 SGM", 8, 10, 16, 30, 17, 30, ReservedSpace.FULL_FIELD);
        var reservationsMarg = Arrays.asList(res_marg_1, res_marg_2, res_marg_3, res_marg_4);
        rasenMargetshöchheim.setReservations(reservationsMarg);
        margetshöchheim.setSportsFacilities(Arrays.asList(rasenMargetshöchheim));
        addressRepository.save(margetshöchheim);

        Reservation res_or_z_1 = buildNewReservation(thomas, "U11-1", 8, 9, 16, 45, 18, 15, ReservedSpace.FULL_FIELD);
        Reservation res_or_z_2 = buildNewReservation(thomas, "U11-1", 8, 11, 16, 45, 18, 15, ReservedSpace.FULL_FIELD);
        Reservation res_or_z_3 = buildNewReservation(thomas, "U9-1", 8, 8, 17, 30, 18, 30, ReservedSpace.FULL_FIELD);
        Reservation res_or_z_4 = buildNewReservation(thomas, "U9-3", 8, 10, 17, 0, 18, 30, ReservedSpace.FULL_FIELD);
        res_or_z_4.setReservationPurpose(ReservationPurpose.GAME);
        Reservation res_or_z_5 = buildNewReservation(thomas, "U7 FC", 8, 8, 17, 0, 18, 0, ReservedSpace.FULL_FIELD);
        var reservationsObererRasenZell = Arrays.asList(res_or_z_1, res_or_z_2, res_or_z_3, res_or_z_4, res_or_z_5);
        obererRasenZellAmMain.setReservations(reservationsObererRasenZell);

        Reservation res_ur_z_1 = buildNewReservation(thomas, "U17", 8, 9, 17, 30, 19, 15, ReservedSpace.FULL_FIELD);
        Reservation res_ur_z_2 = buildNewReservation(thomas, "U17", 8, 11, 17, 30, 19, 15, ReservedSpace.FULL_FIELD);

        var reservationsUntererRasenZell = Arrays.asList(res_ur_z_1, res_ur_z_2);
        untererRasenZellAmMain.setReservations(reservationsUntererRasenZell);
        zellAmMain.setSportsFacilities(Arrays.asList(obererRasenZellAmMain, untererRasenZellAmMain));
        addressRepository.save(zellAmMain);
    }

    private static Reservation buildNewReservation(User applicant, String reservationName, int month, int day, int startHour, int startMinute, int endHour, int endMinute, ReservedSpace reservedSpace) {
        Reservation reservation = new Reservation();
        reservation.setName(reservationName);
        reservation.setStartTime(LocalDateTime.of(2022, month, day, startHour, startMinute, 0));
        reservation.setEndTime(LocalDateTime.of(2022, month, day, endHour, endMinute, 0));
        reservation.setApplicant(applicant);
        reservation.setReservationStatus(ReservationStatus.APPROVED);
        reservation.setReservationPurpose(ReservationPurpose.TRAINING);
        reservation.setReservedSpace(reservedSpace);
        return reservation;
    }

    private User buildSupervisor(String name) {
        User supervisor1 = new User();
        supervisor1.setUsername(name);
        supervisor1.setPassword(passwordEncoder.encode("my-pw"));
        supervisor1.setFullname(name);
        supervisor1.setEmail(name+ "-mail@hoster.com");
        supervisor1.setTelephoneNumber("195670836068");
        supervisor1.setRole(Role.CLUB_SUPERVISOR);
        return supervisor1;
    }

    private void oldMockData(User thomas, User sandra, User karl, User richard) {
        //old mock data

        Address address1 = createAddress1TalerAllee(addressRepository);
        Address address2 = createAddress2Frankenstrasse(addressRepository);

        SportsFacility soccerField = createFacilitySoccerFieldForAddress1TalerAllee(addressRepository, richard, address1);
        SportsFacility tennisCourt = createFacilityTennisCourtForAddress2Frankenstrasse(addressRepository, richard, address2);

        createReservationsForSoccerFieldOfAddress1(addressRepository, thomas, address1, soccerField);
        createReservationsForTennisCourtOfAddress2(addressRepository, sandra, karl, address2, tennisCourt);
    }

    public static User createUserAdminTommy(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        User thomas = buildUserAdminTommy(passwordEncoder);
        return userRepository.save(thomas);
    }

    public static User createUserPlayerSanny(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        User sandra = buildUserPlayerSanny(passwordEncoder);
        return userRepository.save(sandra);
    }

    public static User createUserTrainerKalli(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        User karl = buildUserTrainerKalli(passwordEncoder);
        return userRepository.save(karl);
    }

    public static User createUserSupervisorRichard(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        User richard = buildUserSupervisorRichard(passwordEncoder);
        return userRepository.save(richard);
    }
    public static Address createAddress1TalerAllee(AddressRepository addressRepository) {
        Address address = buildAddressTalerAllee();
        return addressRepository.save(address);
    }

    public static Address createAddress2Frankenstrasse(AddressRepository addressRepository) {
        Address address2 = buildAddressFrankenstrasse();
        return addressRepository.save(address2);
    }

    public static SportsFacility createFacilitySoccerFieldForAddress1TalerAllee(AddressRepository addressRepository, User supervisor, Address address) {
        SportsFacility soccerField = buildFacilitySoccerFieldOfAddressTalerAllee(supervisor, address);
        Address savedAddress = addressRepository.save(address);
        address.setSportsFacilities(savedAddress.getSportsFacilities());
        return savedAddress.getSportsFacilities().stream().filter(fac -> Objects.equals(fac.getName(), soccerField.getName())).findFirst().orElseThrow();
    }

    public static SportsFacility createFacilityTennisCourtForAddress2Frankenstrasse(AddressRepository addressRepository, User supervisor, Address address2) {
        SportsFacility tennisCourt = buildFacilityTennisCourtForAddressFrankenstrasse(supervisor, address2);
        Address savedAddress = addressRepository.save(address2);
        address2.setSportsFacilities(savedAddress.getSportsFacilities());
        return savedAddress.getSportsFacilities().stream().filter(fac -> Objects.equals(fac.getName(), tennisCourt.getName())).findFirst().orElseThrow();
    }

    public static List<Reservation> createReservationsForSoccerFieldOfAddress1(AddressRepository addressRepository, User applicant, Address address, SportsFacility soccerField) {
        var reservations = buildReservationsForSportsFacilitySoccerField(applicant);
        soccerField.setReservations(reservations);
        address.getSportsFacilities()
                .replaceAll(fac -> Objects.equals(fac.getName(), soccerField.getName()) ? soccerField : fac);
        Address savedAddress = addressRepository.save(address);
        address.setSportsFacilities(savedAddress.getSportsFacilities());

        SportsFacility updatedFacility = savedAddress.getSportsFacilities()
                .stream()
                .filter(fac -> Objects.equals(fac.getName(), soccerField.getName()))
                .findFirst().orElseThrow();
        return updatedFacility.getReservations();
    }

    public static List<Reservation> createReservationsForTennisCourtOfAddress2(AddressRepository addressRepository, User applicant1, User applicant2, Address address2, SportsFacility tennisCourt) {
        List<Reservation> reservations = buildReservationsForSportsFacilityTennisCourt(applicant1, applicant2);

        tennisCourt.setReservations(reservations);
        address2.getSportsFacilities().replaceAll(fac -> Objects.equals(fac.getName(), tennisCourt.getName()) ? tennisCourt : fac);
        Address savedAddress = addressRepository.save(address2);
        address2.setSportsFacilities(savedAddress.getSportsFacilities());

        SportsFacility updatedFacility = savedAddress.getSportsFacilities().stream().filter(fac -> Objects.equals(fac.getName(), tennisCourt.getName())).findFirst().orElseThrow();
        return updatedFacility.getReservations();
    }


    public static User buildUserAdminTommy(PasswordEncoder passwordEncoder) {
        User thomas = new User();
        thomas.setUsername("tommy");
        thomas.setPassword(passwordEncoder.encode("a_secret_pw"));
        thomas.setFullname("Thomas Schmidt");
        thomas.setEmail("thomas-mail@hoster.com");
        thomas.setTelephoneNumber("123456789");
        thomas.setRole(Role.ADMIN);
        return thomas;
    }
    public static User buildUserPlayerSanny(PasswordEncoder passwordEncoder) {
        User sandra = new User();
        sandra.setUsername("sanny");
        sandra.setPassword(passwordEncoder.encode("another_secret_pw"));
        sandra.setFullname("Sandra Müller");
        sandra.setEmail("sandra-mail@hoster.com");
        sandra.setTelephoneNumber("135792468");
        sandra.setRole(Role.PLAYER);
        return sandra;
    }
    public static User buildUserTrainerKalli(PasswordEncoder passwordEncoder) {
        User karl = new User();
        karl.setUsername("kalli");
        karl.setPassword(passwordEncoder.encode("my-password"));
        karl.setFullname("Karl Huber");
        karl.setEmail("sportsfacility@protonmail.com");
        karl.setTelephoneNumber("135792454668");
        karl.setRole(Role.TRAINER);
        return karl;
    }
    public static User buildUserSupervisorRichard(PasswordEncoder passwordEncoder) {
        User richard = new User();
        richard.setUsername("richie");
        richard.setPassword(passwordEncoder.encode("my-pw"));
        richard.setFullname("Richard Fischer");
        richard.setEmail("rich-mail@hoster.com");
        richard.setTelephoneNumber("195670836068");
        richard.setRole(Role.CLUB_SUPERVISOR);
        return richard;
    }
    public static Address buildAddressTalerAllee() {
        Address address = new Address();
        address.setStreet("Taler-Allee");
        address.setHouseNumber("3a");
        address.setPostalCode(97070);
        address.setLocation("Würzburg");
        address.setSportsFacilities(new ArrayList<>());
        return address;
    }
    public static Address buildAddressFrankenstrasse() {
        Address address2 = new Address();
        address2.setStreet("Frankenstraße");
        address2.setHouseNumber("15");
        address2.setPostalCode(97074);
        address2.setLocation("Veitshöchheim");
        address2.setSportsFacilities(new ArrayList<>());
        return address2;
    }
    public static SportsFacility buildFacilitySoccerFieldOfAddressTalerAllee(User supervisor, Address address) {
        SportsFacility soccerField = new SportsFacility();
        soccerField.setName("Fußballplatz A");
        soccerField.setHasFloodlight(false);
        soccerField.setCourtSurface(CourtSurface.GRASS);
        soccerField.setSupervisors(Arrays.asList(supervisor));
        soccerField.setReservations(new ArrayList<>());
        address.setSportsFacilities(Arrays.asList(soccerField));
        return soccerField;
    }
    public static SportsFacility buildFacilityTennisCourtForAddressFrankenstrasse(User supervisor, Address address2) {
        SportsFacility tennisCourt = new SportsFacility();
        tennisCourt.setName("Tennisplatz");
        tennisCourt.setHasFloodlight(true);
        tennisCourt.setCourtSurface(CourtSurface.ARTIFICIAL_TURF);
        tennisCourt.setSupervisors(Arrays.asList(supervisor));
        tennisCourt.setReservations(new ArrayList<>());
        address2.setSportsFacilities(Arrays.asList(tennisCourt));
        return tennisCourt;
    }
    public static List<Reservation> buildReservationsForSportsFacilitySoccerField(User applicant) {
        Reservation reservation1 = new Reservation();
        reservation1.setName("Leinach Spiel");
        reservation1.setStartTime(LocalDateTime.of(2022, 7, 15, 8, 0, 0));
        reservation1.setEndTime(LocalDateTime.of(2022, 7, 15, 10, 0, 0));
        reservation1.setApplicant(applicant);
        reservation1.setReservationStatus(ReservationStatus.PENDING);
        reservation1.setReservationPurpose(ReservationPurpose.GAME);
        reservation1.setReservedSpace(ReservedSpace.FULL_FIELD);
        return Arrays.asList(reservation1);
    }
    public static List<Reservation> buildReservationsForSportsFacilityTennisCourt(User applicant1, User applicant2) {
        Reservation reservation2 = new Reservation();
        reservation2.setName("Erlabrunner Match");
        reservation2.setStartTime(LocalDateTime.of(2022, 7, 13, 18, 0, 0));
        reservation2.setEndTime(LocalDateTime.of(2022, 7, 13, 20, 0, 0));
        reservation2.setApplicant(applicant1);
        reservation2.setReservationStatus(ReservationStatus.PENDING);
        reservation2.setReservationPurpose(ReservationPurpose.TRAINING);
        reservation2.setReservedSpace(ReservedSpace.HALF_OF_FIELD);

        Reservation reservation3 = new Reservation();
        reservation3.setName("Schweinfurter Bolzen");
        reservation3.setStartTime(LocalDateTime.of(2022, 7, 13, 20, 0, 0));
        reservation3.setEndTime(LocalDateTime.of(2022, 7, 13, 21, 30, 0));
        reservation3.setApplicant(applicant2);
        reservation3.setReservationStatus(ReservationStatus.DENIED);
        reservation3.setReservationPurpose(ReservationPurpose.MISC);
        reservation3.setReservedSpace(ReservedSpace.THIRD_OF_FIELD);

        Reservation reservation4 = new Reservation();
        reservation4.setName("Dittelbrunn");
        reservation4.setStartTime(LocalDateTime.of(2022, 7, 18, 14, 0, 0));
        reservation4.setEndTime(LocalDateTime.of(2022, 7, 18, 17, 30, 0));
        reservation4.setApplicant(applicant1);
        reservation4.setReservationStatus(ReservationStatus.DENIED);
        reservation4.setReservationPurpose(ReservationPurpose.BLOCKED);
        reservation4.setReservedSpace(ReservedSpace.FULL_FIELD);

        Reservation reservation5 = new Reservation();
        reservation5.setName("Üchtelhausen");
        reservation5.setStartTime(LocalDateTime.of(2022, 7, 14, 7, 30, 0));
        reservation5.setEndTime(LocalDateTime.of(2022, 7, 14, 8, 45, 0));
        reservation5.setApplicant(applicant2);
        reservation5.setReservationStatus(ReservationStatus.APPROVED);
        reservation5.setReservationPurpose(ReservationPurpose.RESERVED);
        reservation5.setReservedSpace(ReservedSpace.HALF_OF_FIELD);

        Reservation reservation6 = new Reservation();
        reservation6.setName("Bergrheinfeld");
        reservation6.setStartTime(LocalDateTime.of(2022, 7, 16, 16, 15, 0));
        reservation6.setEndTime(LocalDateTime.of(2022, 7, 16, 18, 45, 0));
        reservation6.setApplicant(applicant2);
        reservation6.setReservationStatus(ReservationStatus.PENDING);
        reservation6.setReservationPurpose(ReservationPurpose.GAME);
        reservation6.setReservedSpace(ReservedSpace.THIRD_OF_FIELD);
        return Arrays.asList(reservation2, reservation3, reservation4, reservation5, reservation6);
    }

}
