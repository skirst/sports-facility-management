package de.fhws.sportsfacilitymanagement.service;

import de.fhws.sportsfacilitymanagement.domain.Address;
import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.SportsFacility;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.dto.AddressDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SportsFacilityDto;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservationStatus;
import de.fhws.sportsfacilitymanagement.domain.enums.ReservedSpace;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.mail.EmailServiceImpl;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.SportsFacilityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AddressService {

    private final AddressRepository addressRepository;
    private final SportsFacilityRepository sportsFacilityRepository;
    private final ReservationRepository reservationRepository;
    private final AuthService authService;
    private final UserService userService;
    private final EmailServiceImpl mailSender;

    public ResponseEntity<List<AddressDto>> findAllAddresses() {
        return ResponseEntity.ok(
                addressRepository.findAll().stream().map(AddressDto::from).toList());
    }

    public ResponseEntity<AddressDto> findSingleAddress(final Long addrId) {
        return addressRepository.findById(addrId)
                .map(AddressDto::from)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<AddressDto> createAddress(final AddressDto addressDto) {
        return ResponseEntity.ok(AddressDto.from(addressRepository.save(Address.from(addressDto))));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<AddressDto> updateAddress(final Long id, final AddressDto newAddressDto) {
        return addressRepository.findById(id)
                .map(addr -> {
                    addr.setStreet(newAddressDto.getStreet());
                    addr.setHouseNumber(newAddressDto.getHouseNumber());
                    addr.setPostalCode(newAddressDto.getPostalCode());
                    addr.setLocation(newAddressDto.getLocation());
                    return addressRepository.save(addr);
                })
                .map(AddressDto::from)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteAddress(final Long addrId) {
        addressRepository.deleteById(addrId);
        return ResponseEntity.ok().build();
    }

    // ###### SPORTS FACILITIES ######

    public ResponseEntity<List<SportsFacilityDto>> findAllSportsFacilitiesOfASingleAddress(final Long addrId) {
        return addressRepository.findById(addrId)
                .map((add) -> add.getSportsFacilities().stream()
                        .map(SportsFacilityDto::from).collect(Collectors.toList())
                )
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    public ResponseEntity<SportsFacilityDto> findSingleSportsFacilityOfASingleAddress(final Long addrId, final Long facId) {
        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(SportsFacilityDto::from)
                        .map(ResponseEntity::ok)
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<SportsFacilityDto> createSportsFacilityOfAddress(final Long addrId, SportsFacilityDto newFacilityDto) {
        SportsFacility newFacility = SportsFacility.from(newFacilityDto);
        var supervisors = newFacility.getSupervisors();
        if (supervisors.size() < 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A new facility has to have at least 1 supervisor!");
        }
        var supervisorFromDB = userService.findById(supervisors.get(0).getId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Supervisor doesn't exist!"));
        var userFromDBRole = supervisorFromDB.getRole();
        if (userFromDBRole != Role.CLUB_SUPERVISOR && userFromDBRole != Role.ADMIN) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User isn't a Supervisor!");
        }
        newFacility.setSupervisors(new ArrayList<>(Arrays.asList(supervisorFromDB)));

        return addressRepository.findById(addrId)
                .map(addr -> {
                    SportsFacility newFac = sportsFacilityRepository.save(newFacility);
                    addr.getSportsFacilities().add(newFac);
                    addressRepository.save(addr);
                    return newFac;
                })
                .map(SportsFacilityDto::from)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<SportsFacilityDto> updateSportsFacilityOfAddress(final Long addrId, final Long facId, final SportsFacilityDto newFacilityDto) {
        SportsFacility newFacility = SportsFacility.from(newFacilityDto);
        var supervisors = newFacility.getSupervisors();
        if (supervisors.size() < 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A facility has to have at least 1 supervisor!");
        }
        List<User> updatedSupervisors = new ArrayList<>();
        for (User supervisor : supervisors) {
            var supervisorFromDB = userService.findById(supervisor.getId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "One supervisor user doesn't exist!"));
            var userFromDBRole = supervisorFromDB.getRole();
            if (userFromDBRole != Role.CLUB_SUPERVISOR && userFromDBRole != Role.ADMIN) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "One of the users isn't a Supervisor!");
            }
            updatedSupervisors.add(supervisorFromDB);
        }

        newFacility.setSupervisors(updatedSupervisors);
        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(fac -> {
                            fac.setName(newFacility.getName());
                            fac.setHasFloodlight(newFacility.isHasFloodlight());
                            fac.setCourtSurface(newFacility.getCourtSurface());
                            fac.setSupervisors(newFacility.getSupervisors());
                            return sportsFacilityRepository.save(fac);
                        })
                        .map(SportsFacilityDto::from)
                        .map(ResponseEntity::ok)
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteSportsFacilityOfAddress(final Long addrId, final Long facId) {
        return addressRepository.findById(addrId)
                .map(addr -> {
                    SportsFacility facToDelete = sportsFacilityRepository.findById(facId)
                            .orElseThrow(EntityNotFoundException::new);
                    addr.getSportsFacilities().remove(facToDelete);
                    addressRepository.save(addr);
                    return ResponseEntity.ok().build();
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // ###### RESERVATIONS ######

    // Calendar view, doesn't show denied reservations
    public ResponseEntity<List<Reservation>> getCalendarView(final Long addrId, final Long facId) {
        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(SportsFacility::getReservations)
                        .map(reserv ->  reserv.stream()
                                    .filter(r -> r.getReservationStatus() != ReservationStatus.DENIED)
                                    .collect(Collectors.toList())
                        )
                        .map(ResponseEntity::ok)
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // role-specific views (PLAYER: no view, TRAINER: only own reservations, SUPERVISOR of this facility/ADMIN: all reservations
    public ResponseEntity<List<Reservation>> findAllReservationsOfSingleSportsFacilityOfSingleAddress(final Long addrId, final Long facId) {
        var user = authService.getUserDetails().orElseThrow(() -> new UsernameNotFoundException("Could not find user."));

        // show no DENIED reservations (for FE calendar)
        // show a user only his/her reservations (PENDING/APPROVED + his/her DENIED) -> param: user-view
        // show supervisor all reservations of the sports facility he/she is responsible for (PENDING/APPROVED/DENIED) -> check role
        // show admin all reservations

        // check if user = supervisor of this facility
        var isSupervisorOfFacility = addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .stream()
                        .anyMatch(sportsFacility -> sportsFacility
                                .getSupervisors().stream()
                                .anyMatch(superv -> Objects.equals(superv.getId(), user.getId()))
                        )
                ).orElse(false);

        var isUserSupervisor = (user.getRole() == Role.CLUB_SUPERVISOR) && (isSupervisorOfFacility);

        if (!isUserSupervisor && user.getRole() != Role.ADMIN) {
            log.info("User is neither supervisor of this sports facility nor admin.");
            // return only APPROVED/PENDING + user-owned DENIED reservations
            return addressRepository.findById(addrId)
                    .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                            .map(SportsFacility::getReservations)
                            .map(reserv -> reserv.stream()
                                        .filter(r -> Objects.equals(r.getApplicant().getId(), user.getId()))
                                        .toList()
                            )
                            .map(ResponseEntity::ok)
                            .orElseGet(() -> ResponseEntity.notFound().build())
                    )
                    .orElseGet(() -> ResponseEntity.notFound().build());
        }
        log.info("User is supervisor of this sports facility or admin.");
        // return all reservations to supervisors and admins
        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(SportsFacility::getReservations)
                        .map(ResponseEntity::ok)
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    public ResponseEntity<Reservation> findSingleReservationOfSingleSportsFacilityOfSingleAddress(final Long addrId, final Long facId, final Long reservId) {
        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(fac -> findSingleReservation(reservId, fac.getReservations())
                                .map(ResponseEntity::ok)
                                .orElseGet(() -> ResponseEntity.notFound().build())
                        )
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PreAuthorize("hasRole('TRAINER') or hasRole('ADMIN')")
    public ResponseEntity<Reservation> createSingleReservationOfSingleSportsFacility(final Long addrId, final Long facId, final Reservation reservation) {
        var user = authService.getUserDetails().orElseThrow(() -> new UsernameNotFoundException("Could not find user."));

        var reservToSave = new Reservation();
        reservToSave.setName(reservation.getName());
        // check for time collisions with other reservations at the same time
        boolean isReservationPossible = isReservationPossible(facId, reservation);
        if (!isReservationPossible) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot reserve the requested space in the given time frame.");
        }
        reservToSave.setReservedSpace(reservation.getReservedSpace());
        reservToSave.setStartTime(reservation.getStartTime());
        reservToSave.setEndTime(reservation.getEndTime());
        reservToSave.setApplicant(user);
        reservToSave.setReservationStatus(ReservationStatus.PENDING);
        reservToSave.setReservationPurpose(reservation.getReservationPurpose());

        return addressRepository.findById(addrId)
                .map(addr -> {
                            SportsFacility fac = findSingleSportsFacility(facId, addr.getSportsFacilities())
                                    .orElseThrow(EntityNotFoundException::new);
                            Reservation reserv = reservationRepository.save(reservToSave);
                            fac.getReservations().add(reserv);
                            addressRepository.save(addr);

                            // send notification mail to each supervisor of the facility
                            var subject = mailSender.createNewReservationSubject(reservToSave.getReservationPurpose().toString(), fac.getName());
                            var message = mailSender.createNewReservationMessage(reservToSave, fac.getName());
                            fac.getSupervisors().forEach(supervisor -> mailSender.sendSimpleMessage(
                                    supervisor.getEmail(),
                                    subject,
                                    message
                                    ));

                            return reserv;
                })
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private boolean isReservationPossible(Long facId, Reservation reservation) {
        var collidingReservations = reservationRepository.findAllTimeCollisions(facId, reservation.getStartTime(), reservation.getEndTime());
        if (reservation.getId() != null) {
            //remove reservation from collisions in case it already is in db (e.g. this happens when we change the reservationStatus)
            collidingReservations = collidingReservations.stream().filter(r -> !Objects.equals(r.getId(), reservation.getId())).toList();
        }
        var time = reservation.getStartTime();
        var endTime = reservation.getEndTime();
        var neededSpace = reservation.getReservedSpace();
        // check if new reservation collides with existing ones (space + time slots) - 15min interval checks
        // e.g. new reservation: 08:00 - 09:30, needs half of the field
        // -> check if enough space is available at 08:00, 08:15, ...
        while (time.isBefore(endTime) || time.isEqual(endTime)) {
            var remainingFreeSpace = getRemainingFreeSpace(time, collidingReservations);
            if (!remainingFreeSpace.contains(neededSpace)) {
                return false;
            }
            time = time.plusMinutes(15);
        }
        return true;
    }

    // admin can update all fields of a reservation
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Reservation> updateSingleReservationOfSingleSportsFacility(final Long addrId, final Long facId, final Long reservId, final Reservation updatedReservation) {

        return addressRepository.findById(addrId)
                .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                        .map(fac -> findSingleReservation(reservId, fac.getReservations())
                                .map(oldReserv -> {
                                    oldReserv.setStartTime(updatedReservation.getStartTime());
                                    oldReserv.setEndTime(updatedReservation.getEndTime());
                                    oldReserv.setApplicant(updatedReservation.getApplicant());
                                    oldReserv.setReservationStatus(updatedReservation.getReservationStatus());
                                    oldReserv.setReservationPurpose(updatedReservation.getReservationPurpose());
                                    return reservationRepository.save(oldReserv);
                                })
                                .map(ResponseEntity::ok)
                                .orElseGet(() -> ResponseEntity.notFound().build())
                        )
                        .orElseGet(() -> ResponseEntity.notFound().build())
                )
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // supervisor can only update the status of the request (user requests, supervisor approves/denies the pending request)
    @PreAuthorize("hasRole('CLUB_SUPERVISOR') or hasRole('ADMIN')")
    public ResponseEntity<Reservation> updateReservationStatus(final Long addrId, final Long facId, final Long reservId, final ReservationStatus newStatus) {
        var user = authService.getUserDetails().orElseThrow(() -> new UsernameNotFoundException("Could not find user."));

        var isSupervisorOfFacility = false;
        // check if user = supervisor of this facility
        if (user.getRole() == Role.CLUB_SUPERVISOR) {
            isSupervisorOfFacility = addressRepository.findById(addrId)
                    .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                            .stream()
                            .anyMatch(sportsFacility -> sportsFacility
                                    .getSupervisors().stream()
                                    .anyMatch(superv -> Objects.equals(superv.getId(), user.getId()))
                            )
                    ).orElse(false);
        }
        if (isSupervisorOfFacility || user.getRole() == Role.ADMIN) {
            return addressRepository.findById(addrId)
                    .map(addr -> findSingleSportsFacility(facId, addr.getSportsFacilities())
                            .map(fac -> findSingleReservation(reservId, fac.getReservations())
                                    .map(oldReserv -> {
                                        if (oldReserv.getReservationStatus() == newStatus) {
                                            //no change just return
                                            return oldReserv;
                                        }
                                        if (newStatus == ReservationStatus.APPROVED && !isReservationPossible(facId, oldReserv)) {
                                            //found collisions
                                            throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot set status of reservation to approved. Reservation conflicts with other already approved reservations!");
                                        }
                                        oldReserv.setReservationStatus(newStatus);

                                        // notify applicant of the update
                                        mailSender.sendSimpleMessage(
                                                oldReserv.getApplicant().getEmail(),
                                                "Your reservation was " + oldReserv.getReservationStatus(),
                                                mailSender.createReservationStatusUpdateMessage(oldReserv, fac.getName())
                                        );

                                        return reservationRepository.save(oldReserv);
                                    })
                                    .map(ResponseEntity::ok)
                                    .orElseGet(() -> ResponseEntity.notFound().build())
                            )
                            .orElseGet(() -> ResponseEntity.notFound().build())
                    )
                    .orElseGet(() -> ResponseEntity.notFound().build());
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    @PreAuthorize("hasRole('TRAINER') or hasRole('ADMIN')")
    public ResponseEntity<Object> deleteSingleReservationOfSportsFacilityOfAddress(final Long addrId, final Long facId, final Long reservId) {
        var user = authService.getUserDetails().orElseThrow(() -> new UsernameNotFoundException("Could not find user."));

        return addressRepository.findById(addrId)
                .map(addr -> {
                    SportsFacility fac = findSingleSportsFacility(facId, addr.getSportsFacilities())
                            .orElseThrow(EntityNotFoundException::new);
                    Reservation reservToDelete = findSingleReservation(reservId, fac.getReservations())
                            .orElseThrow(EntityNotFoundException::new);

                    // only admin can delete reservations of other users
                    // trainer role can only delete his/her own reservations
                    if ((!Objects.equals(reservToDelete.getApplicant().getId(), user.getId())) && (user.getRole() != Role.ADMIN)) {
                        throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Cannot delete reservation of another user.");
                    }

                    fac.getReservations().remove(reservToDelete);
                    addressRepository.save(addr);
                    return ResponseEntity.ok().build();
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Helper methods

    private static Optional<SportsFacility> findSingleSportsFacility(final Long facId, final List<SportsFacility> sportsFacilities) {
        return sportsFacilities.stream()
                .filter(fac -> Objects.equals(fac.getId(), facId))
                .findFirst();
    }

    private static Optional<Reservation> findSingleReservation(final Long reservId, final List<Reservation> reservations) {
        return reservations.stream()
                .filter(reserv -> Objects.equals(reserv.getId(), reservId))
                .findFirst();
    }

    private static Set<ReservedSpace> getRemainingFreeSpace(final LocalDateTime pointInTime, final List<Reservation> collidingReservations) {
        var allSpacesFree = Set.of(ReservedSpace.FULL_FIELD, ReservedSpace.HALF_OF_FIELD, ReservedSpace.THIRD_OF_FIELD);
        if (collidingReservations.isEmpty()) {
            return allSpacesFree;
        }

        // get used space at a certain point in time
        var usedSpaces = collidingReservations.stream()
                // only consider approved reservations as collisions
                .filter(reserv -> reserv.getReservationStatus() == ReservationStatus.APPROVED)
                .filter(reserv -> {
                    var startTime = reserv.getStartTime();
                    var endTime = reserv.getEndTime();
                    var startTimeCollision = pointInTime.isAfter(startTime) || pointInTime.isEqual(startTime);
                    var endTimeCollision = pointInTime.isBefore(endTime); // no collision if date == endTime
                    return startTimeCollision && endTimeCollision;
                })
                .map(Reservation::getReservedSpace)
                .toList();
        int usedSpaceCounter = 0;
        for (ReservedSpace space : usedSpaces) {
            if (space.equals(ReservedSpace.FULL_FIELD)) {
                usedSpaceCounter += 100;
            } else if (space.equals(ReservedSpace.HALF_OF_FIELD)) {
                usedSpaceCounter += 50;
            } else if (space.equals(ReservedSpace.THIRD_OF_FIELD)) {
                usedSpaceCounter += 33;
            }
        }
        var result = new HashSet<ReservedSpace>();
        if (usedSpaceCounter <= 66) {
            result.add(ReservedSpace.THIRD_OF_FIELD);
        }
        if (usedSpaceCounter <= 50) {
            result.add(ReservedSpace.HALF_OF_FIELD);
        }
        if (usedSpaceCounter == 0) {
            result.add(ReservedSpace.FULL_FIELD);
        }
        return result;
    }

}
