package de.fhws.sportsfacilitymanagement.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import de.fhws.sportsfacilitymanagement.domain.SecurityUserDetails;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import de.fhws.sportsfacilitymanagement.security.JwtUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static de.fhws.sportsfacilitymanagement.common.Constants.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@AllArgsConstructor
@Slf4j
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final JwtUtil jwtUtil;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        log.info("Searching for user {} in the database.", username);
        var user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Could not find user with username = " + username));
        return new SecurityUserDetails(user);
    }


    public Optional<User> getUserDetails() {
        String username = (String) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return userRepository.findByUsername(username);
    }

    public void refreshToken(final HttpServletRequest request, HttpServletResponse response) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String refreshToken = authorizationHeader.substring("Bearer ".length());
            DecodedJWT decodedToken = jwtUtil.getDecodedToken(refreshToken, request);
            String username = decodedToken.getSubject();
            var user = this.loadUserByUsername(username);
            String newAccessToken = jwtUtil.generateToken(request, username, user.getAuthorities(), AUTH_TOKEN_EXPIRATION);
            response.setHeader(AUTHORIZATION, newAccessToken);
            String newRefreshToken = jwtUtil.generateToken(request, username, user.getAuthorities(), REFRESH_TOKEN_EXPIRATION);
            response.setHeader(REFRESH_TOKEN_HEADER, newRefreshToken);
        } else {
            throw new RuntimeException("RefreshToken is missing");
        }
    }

}
