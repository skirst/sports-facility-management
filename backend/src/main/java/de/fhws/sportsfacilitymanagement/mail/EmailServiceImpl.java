package de.fhws.sportsfacilitymanagement.mail;

import de.fhws.sportsfacilitymanagement.domain.Reservation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Component
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender emailSender;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
    @Value("${spring.mail.username}")
    private String mailUsername;

    //sending an email can take up to 5s, so using async let's us proceed non-blocking
    @Override @Async
    public void sendSimpleMessage(final String to, final String subject, final String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailUsername);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        try {
            emailSender.send(message);
        } catch(MailException e) {
            log.warn("Failed sending email: " + e.getMessage());
        }

    }

    public String createNewReservationSubject(final String reservationPurpose, final String facilityName) {
        return "New reservation request (" + reservationPurpose + " at " + facilityName + ")";
    }

    public String createNewReservationMessage(final Reservation reservation, final String facilityName) {
        return "A new reservation for the sports facility " + facilityName +
                " for the time between " + reservation.getStartTime().format(formatter) +
                " and " + reservation.getEndTime().format(formatter) + " has been requested.\n" +
                "The needed space is " + reservation.getReservedSpace() +
                " and the purpose is " + reservation.getReservationPurpose() + ".";
    }

    public String createReservationStatusUpdateMessage(final Reservation reservation, final String facilityName) {
        return "The status of your reservation for the sports facility " + facilityName +
                " for the time between " + reservation.getStartTime().format(formatter) +
                " and " + reservation.getEndTime().format(formatter) + " has been updated to " + reservation.getReservationStatus() + ".";
    }

}
