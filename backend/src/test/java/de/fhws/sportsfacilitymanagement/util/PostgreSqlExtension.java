package de.fhws.sportsfacilitymanagement.util;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.PostgreSQLContainer;

public class PostgreSqlExtension implements BeforeAllCallback, AfterAllCallback {

    private static final String IMAGE_VERSION = "postgres:14.1";

    private PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(IMAGE_VERSION);

    @Override
    public void beforeAll(ExtensionContext extensionContext) {
        postgres.start();

        System.setProperty("TEST_DB_URL", postgres.getJdbcUrl());
        System.setProperty("TEST_DB_USERNAME", postgres.getUsername());
        System.setProperty("TEST_DB_PASSWORD", postgres.getPassword());

    }

    @Override
    public void afterAll(ExtensionContext extensionContext) {
        postgres.close();
    }

}
