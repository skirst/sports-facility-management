package de.fhws.sportsfacilitymanagement.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhws.sportsfacilitymanagement.config.PostgreSqlTestcontainersTest;
import de.fhws.sportsfacilitymanagement.domain.Reservation;
import de.fhws.sportsfacilitymanagement.domain.dto.AddressDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SportsFacilityDto;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.SportsFacilityRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import de.fhws.sportsfacilitymanagement.service.MockDataService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import static de.fhws.sportsfacilitymanagement.integration.ITestsUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@PostgreSqlTestcontainersTest
@Slf4j
class AddressITests {

    @Autowired private MockMvc mvc;
    @Autowired private AddressRepository addressRepository;
    @Autowired private SportsFacilityRepository sportsFacilityRepository;
    @Autowired private ReservationRepository reservationRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        clearDB(addressRepository, sportsFacilityRepository, reservationRepository, userRepository);
    }

    //also execute for "TRAINER", "CLUB_SUPERVISOR", "PLAYER"
    //roles is just an aggregation, like read, write, execute
    //we still have to run each test as individual for each role allowed
    @Test @WithMockUser(roles = {"ADMIN"})
    void admin_shouldExecuteRequests() throws Exception {
        authedUserShouldExecuteGetRequests();
    }

    @Test @WithMockUser(roles = {"TRAINER"})
    void trainer_shouldExecuteRequests() throws Exception {
        authedUserShouldExecuteGetRequests();
    }

    @Test @WithMockUser(roles = {"CLUB_SUPERVISOR"})
    void supervisor_shouldExecuteRequests() throws Exception {
        authedUserShouldExecuteGetRequests();
    }

    @Test @WithMockUser(roles = {"PLAYER"})
    void player_shouldExecuteRequests() throws Exception {
        authedUserShouldExecuteGetRequests();
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldCreateAddress() throws Exception {
        MvcResult res = tryCreateAddress(status().isOk());
        var entityFromDB = getDeserializedClass(res, AddressDto.class, objectMapper);
        assertThat(entityFromDB.getStreet()).isEqualTo("Frankenstraße");
        assertThat(entityFromDB.getHouseNumber()).isEqualTo("15");
        assertThat(entityFromDB.getPostalCode()).isEqualTo(97074);
        assertThat(entityFromDB.getLocation()).isEqualTo("Veitshöchheim");
        assertThat(addressRepository.findById(entityFromDB.getId()).isPresent()).isTrue();
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntCreateAddress() throws Exception {
        tryCreateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntCreateAddress() throws Exception {
        tryCreateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntCreateAddress() throws Exception {
        tryCreateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldUpdateAddress() throws Exception {
        var res = tryUpdateAddress(status().isOk());
        var entityFromDB = getDeserializedClass(res, AddressDto.class, objectMapper);
        assertThat(entityFromDB.getStreet()).isEqualTo("Eichhornstrasse");
        assertThat(entityFromDB.getHouseNumber()).isEqualTo("333b");
        assertThat(entityFromDB.getPostalCode()).isEqualTo(97365);
        assertThat(entityFromDB.getLocation()).isEqualTo("Bamberg");
        assertThat(addressRepository.findById(entityFromDB.getId()).isPresent()).isTrue();
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntUpdateAddress() throws Exception {
        tryUpdateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntUpdateAddress() throws Exception {
        tryUpdateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntUpdateAddress() throws Exception {
        tryUpdateAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldDeleteAddress() throws Exception {
        Long deleteAddressId = tryDeleteAddress(status().isOk());
        assertThat(addressRepository.findById(deleteAddressId).isEmpty()).isTrue();
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntDeleteAddress() throws Exception {
        tryDeleteAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntDeleteAddress() throws Exception {
        tryDeleteAddress(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntDeleteAddress() throws Exception {
        tryDeleteAddress(status().isForbidden());
    }

    @NotNull
    private MvcResult tryCreateAddress(ResultMatcher expectedStatus) throws Exception {
        var newAddress = AddressDto.from(MockDataService.buildAddressFrankenstrasse());
        return executePostAndReturnRes("/addresses", newAddress, expectedStatus, mvc);
    }

    private MvcResult tryUpdateAddress(ResultMatcher expectedStatus) throws Exception {
        var addressToUpdate = AddressDto.from(MockDataService.createAddress2Frankenstrasse(addressRepository));
        addressToUpdate.setHouseNumber("333b");
        addressToUpdate.setLocation("Bamberg");
        addressToUpdate.setStreet("Eichhornstrasse");
        addressToUpdate.setPostalCode(97365);
        var path = "/addresses/" + addressToUpdate.getId();
        return executePutAndReturnRes(path, addressToUpdate, expectedStatus, mvc);
    }

    private Long tryDeleteAddress(ResultMatcher expectedStatus) throws Exception {
        var addressToDelete = MockDataService.createAddress2Frankenstrasse(addressRepository);
        var path = "/addresses/" + addressToDelete.getId();
        executeDeleteAndReturnRes(path, expectedStatus, mvc);
        return addressToDelete.getId();
    }

    /**
     * Collection of methods that are accessible to all 4 Roles (GET_ALL, GET)
     */
    private void authedUserShouldExecuteGetRequests() throws Exception {
        //addresses
        authedUserShouldFindAddresses(0);
        var address1TalerAllee = MockDataService.createAddress1TalerAllee(addressRepository);
        var address2Frankenstrasse = MockDataService.createAddress2Frankenstrasse(addressRepository);
        authedUserShouldFindAddresses(2);
        authedUserShouldFindAddress(address1TalerAllee.getId());
        //facilities
        authedUserShouldFindFacilities(address1TalerAllee.getId(), 0);
        var richard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var facilitySoccerField = MockDataService.createFacilitySoccerFieldForAddress1TalerAllee(addressRepository, richard, address1TalerAllee);
        authedUserShouldFindFacilities(address1TalerAllee.getId(), 1);
        authedUserShouldFindFacility(address1TalerAllee.getId(), facilitySoccerField.getId());
        //reservations
        authedUserShouldFindCalendarReservations(address1TalerAllee.getId(), facilitySoccerField.getId(), 0);
        var kalli = MockDataService.createUserTrainerKalli(userRepository, passwordEncoder);
        var reservsOfSoccerField = MockDataService.createReservationsForSoccerFieldOfAddress1(addressRepository, kalli, address1TalerAllee, facilitySoccerField);
        authedUserShouldFindCalendarReservations(address1TalerAllee.getId(), facilitySoccerField.getId(), 1);
        authedUserShouldFindReservation(address1TalerAllee.getId(), facilitySoccerField.getId(), reservsOfSoccerField.get(0).getId());
    }

    private void authedUserShouldFindAddresses(long expectedEntityCount) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses", status().isOk(), mvc);
        var entities = getDeserializedClass(res, AddressDto[].class, objectMapper);
        assertThat(entities.length).isEqualTo(expectedEntityCount);
    }

    private void authedUserShouldFindAddress(long addressId) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses/"+ addressId, status().isOk(), mvc);
        var entity = getDeserializedClass(res, AddressDto.class, objectMapper);
        assertThat(entity.getId()).isEqualTo(addressId);
    }

    private void authedUserShouldFindFacilities(long addressId, long expectedEntityCount) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses/" + addressId + "/sportsfacilities", status().isOk(), mvc);
        var entities = getDeserializedClass(res, SportsFacilityDto[].class, objectMapper);
        assertThat(entities.length).isEqualTo(expectedEntityCount);
    }

    private void authedUserShouldFindFacility(long addressId, long facilityId) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses/" + addressId + "/sportsfacilities/" + facilityId, status().isOk(), mvc);
        var entity = getDeserializedClass(res, SportsFacilityDto.class, objectMapper);
        assertThat(entity.getId()).isEqualTo(facilityId);
    }

    private void authedUserShouldFindCalendarReservations(long addressId, long facilityId, long expectedEntityCount) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses/" + addressId + "/sportsfacilities/" + facilityId + "/calendar", status().isOk(), mvc);
        var entities = getDeserializedClass(res, Reservation[].class, objectMapper);
        assertThat(entities.length).isEqualTo(expectedEntityCount);
    }

    private void authedUserShouldFindReservation(long addressId, long facilityId, long reservationId) throws Exception {
        MvcResult res = executeGetAndReturnRes("/addresses/" + addressId + "/sportsfacilities/" + facilityId + "/reservations/" + reservationId, status().isOk(), mvc);
        var entity = getDeserializedClass(res, Reservation.class, objectMapper);
        assertThat(entity.getId()).isEqualTo(reservationId);
    }

}