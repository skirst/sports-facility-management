package de.fhws.sportsfacilitymanagement.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhws.sportsfacilitymanagement.config.PostgreSqlTestcontainersTest;
import de.fhws.sportsfacilitymanagement.domain.User;
import de.fhws.sportsfacilitymanagement.domain.dto.UserReadDto;
import de.fhws.sportsfacilitymanagement.domain.dto.UserWriteDto;
import de.fhws.sportsfacilitymanagement.domain.enums.Role;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.SportsFacilityRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import de.fhws.sportsfacilitymanagement.service.MockDataService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static de.fhws.sportsfacilitymanagement.integration.ITestsUtils.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@PostgreSqlTestcontainersTest
@Slf4j
class UserITests {

    @Autowired private MockMvc mvc;
    @Autowired private AddressRepository addressRepository;
    @Autowired private SportsFacilityRepository sportsFacilityRepository;
    @Autowired private ReservationRepository reservationRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        clearDB(addressRepository, sportsFacilityRepository, reservationRepository, userRepository);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldGetUsers() throws Exception {
        var res = tryGetUsers(status().isOk());
        var entities = getDeserializedClass(res, UserReadDto[].class, objectMapper);
        assertThat(entities.length).isEqualTo(2);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntGetUsers() throws Exception {
        tryGetUsers(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntGetUsers() throws Exception {
        tryGetUsers(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntGetUsers() throws Exception {
        tryGetUsers(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldGetUser() throws Exception {
        var userId = tryGetUser(status().isOk());
        assertThat(userRepository.findById(userId).isPresent()).isTrue();
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntGetUser() throws Exception {
        tryGetUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntGetUser() throws Exception {
        tryGetUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntGetUser() throws Exception {
        tryGetUser(status().isForbidden());
    }


    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldCreateUser() throws Exception {
        MvcResult res = tryCreateUser(status().isOk());
        var entityFromDB = getDeserializedClass(res, UserWriteDto.class, objectMapper);
        assertThat(entityFromDB.getUsername()).isEqualTo("sanny");
        assertThat(entityFromDB.getFullname()).isEqualTo("Sandra Müller");
        assertThat(entityFromDB.getEmail()).isEqualTo("sandra-mail@hoster.com");
        assertThat(entityFromDB.getTelephoneNumber()).isEqualTo("135792468");
        assertThat(entityFromDB.getRole()).isEqualTo(Role.PLAYER);
        assertThat(userRepository.findById(entityFromDB.getId()).isPresent()).isTrue();
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntCreateUser_password_is_too_short() throws Exception {
        var userToCreate = UserWriteDto.from(MockDataService.buildUserPlayerSanny(passwordEncoder));
        userToCreate.setPassword("short");
        executePostAndReturnRes("/users", userToCreate, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntCreateUser() throws Exception {
        tryCreateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntCreateUser() throws Exception {
        tryCreateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntCreateUser() throws Exception {
        tryCreateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldUpdateUser() throws Exception {
        var res = tryUpdateUser(status().isOk());
        var entityFromDB = getDeserializedClass(res, UserReadDto.class, objectMapper);
        assertThat(entityFromDB.getUsername()).isEqualTo("New Username");
        assertThat(entityFromDB.getFullname()).isEqualTo("New Fullname");
        assertThat(entityFromDB.getTelephoneNumber()).isEqualTo("9999999999");
        assertThat(entityFromDB.getEmail()).isEqualTo("new-email@gmx.de");
        assertThat(entityFromDB.getRole()).isEqualTo(Role.TRAINER);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntUpdateUser_password_is_too_short() throws Exception {
        var userToUpdate = UserWriteDto.from(MockDataService.buildUserPlayerSanny(passwordEncoder));
        userToUpdate.setPassword("short");
        executePutAndReturnRes("/users/" + userToUpdate.getId(), userToUpdate, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntUpdateUser_cant_take_privileges_of_last_admin() throws Exception {
        var userToUpdate = UserWriteDto.from(MockDataService.createUserAdminTommy(userRepository, passwordEncoder));
        userToUpdate.setRole(Role.TRAINER);
        executePutAndReturnRes("/users/" + userToUpdate.getId(), userToUpdate, status().isConflict(), mvc);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntUpdateUser_cant_take_privileges_of_last_supervisor_of_a_facility() throws Exception {
        var userToUpdate = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var address = MockDataService.createAddress1TalerAllee(addressRepository);
        MockDataService.createFacilitySoccerFieldForAddress1TalerAllee(addressRepository, userToUpdate, address);
        var userToUpdateDto = UserWriteDto.from(userToUpdate);
        userToUpdateDto.setRole(Role.TRAINER);
        executePutAndReturnRes("/users/" + userToUpdateDto.getId(), userToUpdateDto, status().isConflict(), mvc);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntUpdateUser() throws Exception {
        tryUpdateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntUpdateUser() throws Exception {
        tryUpdateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntUpdateUser() throws Exception {
        tryUpdateUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldDeleteUser() throws Exception {
        Long deleteAddressId = tryDeleteUser(status().isOk());
        assertThat(userRepository.findById(deleteAddressId).isEmpty()).isTrue();
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntDeleteUser_cant_delete_the_last_admin() throws Exception {
        var userToDelete = MockDataService.createUserAdminTommy(userRepository, passwordEncoder);
        executeDeleteAndReturnRes("/users/" + userToDelete.getId(), status().isConflict(), mvc);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntDeleteUser_cant_delete_the_supervisor_of_a_facility() throws Exception {
        var userToDelete = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var address = MockDataService.createAddress1TalerAllee(addressRepository);
        MockDataService.createFacilitySoccerFieldForAddress1TalerAllee(addressRepository, userToDelete, address);
        executeDeleteAndReturnRes("/users/" + userToDelete.getId(), status().isConflict(), mvc);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldDeleteUser() throws Exception {
        tryDeleteUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldDeleteUser() throws Exception {
        tryDeleteUser(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldDeleteUser() throws Exception {
        tryDeleteUser(status().isForbidden());
    }

    @NotNull
    private MvcResult tryCreateUser(ResultMatcher expectedStatus) throws Exception {
        var userToCreate = UserWriteDto.from(MockDataService.buildUserPlayerSanny(passwordEncoder));
        return executePostAndReturnRes("/users", userToCreate, expectedStatus, mvc);
    }

    private MvcResult tryUpdateUser(ResultMatcher expectedStatus) throws Exception {
        var userToUpdate = UserWriteDto.from(MockDataService.createUserPlayerSanny(userRepository, passwordEncoder));
        userToUpdate.setUsername("New Username");
        userToUpdate.setFullname("New Fullname");
        userToUpdate.setTelephoneNumber("9999999999");
        userToUpdate.setEmail("new-email@gmx.de");
        userToUpdate.setPassword("newPassword");
        userToUpdate.setRole(Role.TRAINER);
        var path = "/users/" + userToUpdate.getId();
        return executePutAndReturnRes(path, userToUpdate, expectedStatus, mvc);
    }

    private Long tryDeleteUser(ResultMatcher expectedStatus) throws Exception {
        var userToDelete = MockDataService.createUserPlayerSanny(userRepository, passwordEncoder);
        var path = "/users/" + userToDelete.getId();
        executeDeleteAndReturnRes(path, expectedStatus, mvc);
        return userToDelete.getId();
    }

    private MvcResult tryGetUsers(ResultMatcher expectedStatus) throws Exception {
        MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        MockDataService.createUserTrainerKalli(userRepository, passwordEncoder);
        return executeGetAndReturnRes("/users", expectedStatus, mvc);
    }

    private Long tryGetUser(ResultMatcher expectedStatus) throws Exception {
        var richard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        executeGetAndReturnRes("/users/"+ richard.getId(), expectedStatus, mvc);
        return richard.getId();
    }

}