package de.fhws.sportsfacilitymanagement.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.SportsFacilityRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

public class ITestsUtils {
    public static void clearDB(
            AddressRepository addressRepository,
            SportsFacilityRepository sportsFacilityRepository,
            ReservationRepository reservationRepository,
            UserRepository userRepository
    ) {
        System.out.println("Cleaning db data for clean test setup!");
        addressRepository.deleteAll();
        System.out.println("Deleted all addresses!");
        sportsFacilityRepository.deleteAll();
        System.out.println("Deleted all facilities!");
        reservationRepository.deleteAll();
        System.out.println("Deleted all reservations!");
        userRepository.deleteAll();
        System.out.println("Deleted all users!");
    }

    @NotNull
    public static MvcResult executeGetAndReturnRes(String url, ResultMatcher status, MockMvc mvc) throws Exception {
        return mvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status)
                .andReturn();
    }
    @NotNull
    public static <T> MvcResult executePutAndReturnRes(String path, T entityToUpdate, ResultMatcher status, MockMvc mvc) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        return mvc.perform(put(path)
                        .content(objectMapper.writeValueAsString(entityToUpdate)).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status)
                .andReturn();
    }

    @NotNull
    public static <T> MvcResult executePostAndReturnRes(String path, T entityToCreate, ResultMatcher status, MockMvc mvc) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        return mvc.perform(post(path)
                        .content(objectMapper.writeValueAsString(entityToCreate)).contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status)
                .andReturn();
    }

    @NotNull
    public static MvcResult executeDeleteAndReturnRes(String path, ResultMatcher status, MockMvc mvc) throws Exception {
        return mvc.perform(delete(path)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(status)
                .andReturn();
    }

    public static <T> T getDeserializedClass(MvcResult res, Class<T> clazz, ObjectMapper objectMapper) throws JsonProcessingException, UnsupportedEncodingException {
        return objectMapper.readValue(
                res.getResponse().getContentAsString(Charset.defaultCharset()),
                clazz);
    }
}
