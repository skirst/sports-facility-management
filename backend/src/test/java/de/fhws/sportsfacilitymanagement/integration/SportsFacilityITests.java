package de.fhws.sportsfacilitymanagement.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhws.sportsfacilitymanagement.config.PostgreSqlTestcontainersTest;
import de.fhws.sportsfacilitymanagement.domain.dto.SportsFacilityDto;
import de.fhws.sportsfacilitymanagement.domain.dto.SupervisorDto;
import de.fhws.sportsfacilitymanagement.domain.enums.CourtSurface;
import de.fhws.sportsfacilitymanagement.repository.AddressRepository;
import de.fhws.sportsfacilitymanagement.repository.ReservationRepository;
import de.fhws.sportsfacilitymanagement.repository.SportsFacilityRepository;
import de.fhws.sportsfacilitymanagement.repository.UserRepository;
import de.fhws.sportsfacilitymanagement.service.MockDataService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;

import static de.fhws.sportsfacilitymanagement.integration.ITestsUtils.*;
import static de.fhws.sportsfacilitymanagement.integration.ITestsUtils.executeDeleteAndReturnRes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
@PostgreSqlTestcontainersTest
@Slf4j
class SportsFacilityITests {

    @Autowired private MockMvc mvc;
    @Autowired private AddressRepository addressRepository;
    @Autowired private SportsFacilityRepository sportsFacilityRepository;
    @Autowired private ReservationRepository reservationRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        clearDB(addressRepository, sportsFacilityRepository, reservationRepository, userRepository);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldCreateSportsFacility() throws Exception {
        MvcResult res = tryCreateSportsFacility(status().isOk());
        var entityFromDB = getDeserializedClass(res, SportsFacilityDto.class, objectMapper);
        assertThat(entityFromDB.getName()).isEqualTo("Fußballplatz A");
        assertThat(entityFromDB.getCourtSurface()).isEqualTo(CourtSurface.GRASS);
        assertThat(entityFromDB.isHasFloodlight()).isEqualTo(false);
        assertThat(entityFromDB.getSupervisors().size()).isEqualTo(1);
        assertThat(entityFromDB.getSupervisors().get(0).getUsername()).isEqualTo("richie");
        assertThat(sportsFacilityRepository.findById(entityFromDB.getId()).isPresent()).isTrue();
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntCreateSportsFacility_sent_supervisors_contains_user_with_insufficient_privileges() throws Exception {
        var insufficientPrivilegesTrainerKalli = MockDataService.createUserTrainerKalli(userRepository, passwordEncoder);
        var talerAllee = MockDataService.createAddress1TalerAllee(addressRepository);
        var newSportsFacility = MockDataService.buildFacilitySoccerFieldOfAddressTalerAllee(insufficientPrivilegesTrainerKalli, talerAllee);
        executePostAndReturnRes("/addresses/" + talerAllee.getId() + "/sportsfacilities", newSportsFacility, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntCreateSportsFacility_sent_supervisors_doesnt_contain_atleast_one_supervisor() throws Exception {
        var talerAllee = MockDataService.createAddress1TalerAllee(addressRepository);
        var newSportsFacility = MockDataService.buildFacilitySoccerFieldOfAddressTalerAllee(null, talerAllee);
        newSportsFacility.setSupervisors(new ArrayList<>());
        executePostAndReturnRes("/addresses/" + talerAllee.getId() + "/sportsfacilities", newSportsFacility, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntCreateSportsFacility() throws Exception {
        tryCreateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntCreateSportsFacility() throws Exception {
        tryCreateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntCreateSportsFacility() throws Exception {
        tryCreateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldUpdateSportsFacility() throws Exception {
        MvcResult res = tryUpdateSportsFacility(status().isOk());
        var entityFromDB = getDeserializedClass(res, SportsFacilityDto.class, objectMapper);
        assertThat(entityFromDB.getName()).isEqualTo("Tennisplatz downgraded");
        assertThat(entityFromDB.isHasFloodlight()).isEqualTo(false);
        assertThat(entityFromDB.getCourtSurface()).isEqualTo(CourtSurface.GRASS);
        assertThat(entityFromDB.getSupervisors().size()).isEqualTo(1);
        assertThat(entityFromDB.getSupervisors().get(0).getUsername()).isEqualTo("richie");
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntUpdateSportsFacility_sent_supervisors_contains_user_with_insufficient_privileges() throws Exception {
        var supervisorRichard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var insufficientPrivilegesTrainerKalli = MockDataService.createUserTrainerKalli(userRepository, passwordEncoder);
        var address = MockDataService.createAddress2Frankenstrasse(addressRepository);
        var sportsfacility = MockDataService.createFacilityTennisCourtForAddress2Frankenstrasse(addressRepository, supervisorRichard, address);
        var sportsFacilityToUpdate = SportsFacilityDto.from(sportsfacility);
        sportsFacilityToUpdate.setName("Tennisplatz downgraded");
        sportsFacilityToUpdate.setHasFloodlight(false);
        sportsFacilityToUpdate.setCourtSurface(CourtSurface.GRASS);
        sportsFacilityToUpdate.setSupervisors(Arrays.asList(SupervisorDto.from(insufficientPrivilegesTrainerKalli)));
        var path = "/addresses/" + address.getId() + "/sportsfacilities/" + sportsFacilityToUpdate.getId();
        executePutAndReturnRes(path, sportsFacilityToUpdate, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldntUpdateSportsFacility_sent_supervisors_doesnt_contain_atleast_one_supervisor() throws Exception {
        var supervisorRichard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var address = MockDataService.createAddress2Frankenstrasse(addressRepository);
        var sportsfacility = MockDataService.createFacilityTennisCourtForAddress2Frankenstrasse(addressRepository, supervisorRichard, address);
        var sportsFacilityToUpdate = SportsFacilityDto.from(sportsfacility);
        sportsFacilityToUpdate.setName("Tennisplatz downgraded");
        sportsFacilityToUpdate.setHasFloodlight(false);
        sportsFacilityToUpdate.setCourtSurface(CourtSurface.GRASS);
        sportsFacilityToUpdate.setSupervisors(new ArrayList<>());
        var path = "/addresses/" + address.getId() + "/sportsfacilities/" + sportsFacilityToUpdate.getId();
        executePutAndReturnRes(path, sportsFacilityToUpdate, status().isBadRequest(), mvc);
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntUpdateSportsFacility() throws Exception {
        tryUpdateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntUpdateSportsFacility() throws Exception {
        tryUpdateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntUpdateSportsFacility() throws Exception {
        tryUpdateSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "ADMIN")
    void admin_shouldDeleteSportsFacility() throws Exception {
        Long deleteSportsFacilityId = tryDeleteSportsFacility(status().isOk());
        assertThat(sportsFacilityRepository.findById(deleteSportsFacilityId).isEmpty()).isTrue();
    }

    @Test @WithMockUser(roles = "CLUB_SUPERVISOR")
    void supervisor_shouldntDeleteSportsFacility() throws Exception {
        tryDeleteSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "TRAINER")
    void trainer_shouldntDeleteSportsFacility() throws Exception {
        tryDeleteSportsFacility(status().isForbidden());
    }

    @Test @WithMockUser(roles = "PLAYER")
    void player_shouldntDeleteSportsFacility() throws Exception {
        tryDeleteSportsFacility(status().isForbidden());
    }

    private MvcResult tryCreateSportsFacility(ResultMatcher expectedStatus) throws Exception {
        var supervisorRichard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var talerAllee = MockDataService.createAddress1TalerAllee(addressRepository);
        var newSportsFacility = MockDataService.buildFacilitySoccerFieldOfAddressTalerAllee(supervisorRichard, talerAllee);
        return executePostAndReturnRes("/addresses/" + talerAllee.getId() + "/sportsfacilities", newSportsFacility, expectedStatus, mvc);
    }

    private MvcResult tryUpdateSportsFacility(ResultMatcher expectedStatus) throws Exception {
        var supervisorRichard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var address = MockDataService.createAddress2Frankenstrasse(addressRepository);
        var sportsfacility = MockDataService.createFacilityTennisCourtForAddress2Frankenstrasse(addressRepository, supervisorRichard, address);
        var sportsFacilityToUpdate = SportsFacilityDto.from(sportsfacility);
        sportsFacilityToUpdate.setName("Tennisplatz downgraded");
        sportsFacilityToUpdate.setHasFloodlight(false);
        sportsFacilityToUpdate.setCourtSurface(CourtSurface.GRASS);
        sportsFacilityToUpdate.setSupervisors(Arrays.asList(SupervisorDto.from(supervisorRichard)));
        var path = "/addresses/" + address.getId() + "/sportsfacilities/" + sportsFacilityToUpdate.getId();
        return executePutAndReturnRes(path, sportsFacilityToUpdate, expectedStatus, mvc);
    }

    private Long tryDeleteSportsFacility(ResultMatcher expectedStatus) throws Exception {
        var supervisorRichard = MockDataService.createUserSupervisorRichard(userRepository, passwordEncoder);
        var address = MockDataService.createAddress2Frankenstrasse(addressRepository);
        var sportsfacilityToDelete = MockDataService.createFacilityTennisCourtForAddress2Frankenstrasse(addressRepository, supervisorRichard, address);
        var path = "/addresses/" + address.getId() + "/sportsfacilities/" + sportsfacilityToDelete.getId();
        executeDeleteAndReturnRes(path, expectedStatus, mvc);
        return sportsfacilityToDelete.getId();
    }

}