package de.fhws.sportsfacilitymanagement.config;

import de.fhws.sportsfacilitymanagement.util.PostgreSqlExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static de.fhws.sportsfacilitymanagement.common.Profile.TEST;

@ActiveProfiles(TEST)
@DirtiesContext
//unnecessary because of SpringBootTest
//@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//Unnecessary because of SpringBootTest
//@ContextConfiguration(classes = PostgreSqlTestcontainersConfiguration.class)
@ExtendWith(PostgreSqlExtension.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PostgreSqlTestcontainersTest {
}
