package de.fhws.sportsfacilitymanagement.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootConfiguration
@EnableJpaRepositories("de.fhws.sportsfacilitymanagement.repository")
@EntityScan(basePackages = {"de.fhws.sportsfacilitymanagement.domain"})
public class PostgreSqlTestcontainersConfiguration {
}
