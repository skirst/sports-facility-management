# webapp2

# Steps to io build frontend manually

## Install needed packages manually

- `npm install vue@2.6.14` due to using vue2.0, because of compatibilty reasons

## If you want to run vue commands by using @vue/cli in command line [More Detail](https://www.technig.com/solve-vue-command-not-found-windows/)

- `npm install -g @vue/cli`
- add to environment variables (for Win: C:\Users\UserName\AppData\Roaming\npm)

## To build project, run

- `npm install`
- `cd webapp`
- `npm install`
- `npm run serve`

## To run project via docker [More Detail](https://v2.vuejs.org/v2/cookbook/dockerize-vuejs-app.html)

- `npm install`
- `cd webapp`
- `docker build -t webappvuejs .`
- `docker run -it -p 8080:8080 --rm --name webappvuejs webappvuejs`

## Further notes

- [vue-calendar Documentation, especially how do use with vue3/why not using vue3](https://vcalendar.io/vue-3.html)










## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
