import chaiAsPromised from "chai-as-promised";
import chai, { expect } from "chai";
chai.use(chaiAsPromised);

describe("Sportsfacility-API-Integration-Tests", () => {
  it("should eventually fulfill promise", async () => {
    let result = await expect(promiseWillEventuallyResolve()).to.be.eventually
      .fulfilled;
    console.log(result);
  });

  it("should eventually reject promise", async () => {
    let result = await expect(promiseWillEventuallyReject()).to.be.eventually
      .rejected;
    console.log(result);
  });
});

function promiseWillEventuallyResolve() {
  return new Promise((resolve) => {
    sleepThenCallback(1000, resolve);
  });
}

function promiseWillEventuallyReject() {
  return new Promise((resolve, reject) => {
    sleepThenCallback(1000, reject);
  });
}

function sleepThenCallback(timeInMs, cb) {
  let timer = setTimeout(() => {
    clearTimeout(timer);
    cb();
  }, timeInMs);
}
