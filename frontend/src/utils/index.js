// some functions used in multiple files
import {RESERVATION_PURPOSE, RESERVATION_STATUS, RESERVATION_SPACE} from "@/service/json-api";

export function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

/**
 * generic way to update state arrays when getting single element from backend or updating an element
 */
export function replaceOrInsertElWithId(array, element) {
    let index = array.findIndex((el) => el.id === element.id);
    if (index !== -1) {
        // Vue 2 doesn't know if an element in an array changed and therefor won't update reactive Components (fixed in Vue3)
        // when we use splice, it will notice, but it won't do that for array[index] = element
        // https://v2.vuejs.org/v2/guide/reactivity.html#For-Objects
        array.splice(index, 1, element);
    } else {
        array.push(element);
    }
}

export function getColorForPurpose(purpose) {
    switch(purpose){
        case RESERVATION_PURPOSE.TRAINING: return 'green'
        case RESERVATION_PURPOSE.GAME: return "blue"
        case RESERVATION_PURPOSE.BLOCKED: return "red"
        case RESERVATION_PURPOSE.RESERVED: return "purple"
        case RESERVATION_PURPOSE.MISC: return "pink"
        default: {
            return "black"
        }
    }
}

export function getTextColorForStatus(status) {
    switch(status){
        case RESERVATION_STATUS.APPROVED: return 'white'
        case RESERVATION_STATUS.PENDING: return "grey darken-2"
        default: {
            return "black"
        }
    }
}

export function getSpaceForName(name, space) {
    switch(space){
        case RESERVATION_SPACE.FULL_OF_FIELD: return (name + ' (3/3)')
        case RESERVATION_SPACE.HALF_OF_FIELD: return (name + " (1,5/3)")
        case RESERVATION_SPACE.THIRD_OF_FIELD: return (name + " (1/3)")
        default: {
            return name
        }
    }
}

// to get dates working
export function addTimezoneOffset(tempTime){
    let time = new Date(tempTime);
    return new Date(time.getTime() - time.getTimezoneOffset() * 60000).toLocaleString('sv').slice(0, 16);
}

export function getEvents (reservations) {
    const events = []
    for (let i of reservations) {
      let tempColor = getColorForPurpose(i.reservationPurpose);
      let tempTextColor = getTextColorForStatus(i.reservationStatus);
      events.push({
          name: getSpaceForName(i.name, i.reservedSpace),
          onlyName: i.name,
          id: i.id,
          start: i.startTime,
          end: i.endTime,
          color: tempColor,
          textColor: tempTextColor,
          purpose: i.reservationPurpose,
          pending: i.reservationStatus,
          ownerId: i.applicant.id,
          ownerPhone: i.applicant.telephoneNumber,
          ownerUsername: i.applicant.username,
          //timed: !allDay, Ganztagesreservierung!
      })
    }
    return events;
  }