import httpClient from "@/service/my-axios";

/**
 * all Roles
 */
export async function getAllAddresses() {
    return await httpClient.get("/addresses");
}

/**
 * admin
 */export async function postAddress(address) {
    return await httpClient.post(`/addresses`, address);
}

/**
 * admin
 */
export async function putAddress(address) {
    return await httpClient.put(
        `/addresses/${address.id}`,
        address
    );
}

/**
 * admin
 */
export async function deleteAddress(addressId) {
    return await httpClient.delete(`/addresses/${addressId}`);
}

/**
 * all Roles
 */
export async function getAllSportsFacilitiesOfAddress(addressId) {
    return await httpClient.get(`/addresses/${addressId}/sportsfacilities`);
}

/**
 * admin
 */
export async function postSportsFacilityOfAddress(addressId, sportsFacility) {
    return await httpClient.post(`/addresses/${addressId}/sportsfacilities`, sportsFacility);
}

/**
 * admin
 */
export async function putSportsFacilityOfAddress(addressId, sportsFacility) {
    return await httpClient.put(
        `/addresses/${addressId}/sportsfacilities/${sportsFacility.id}`,
        sportsFacility
    );
}

/**
 * admin
 */
export async function deleteSportsFacilityOfAddress(addressId, facilityId) {
    return await httpClient.delete(`/addresses/${addressId}/sportsfacilities/${facilityId}`);
}

/**
 * returns role specific reservations (admin, supervisor, trainer)
 */
export async function getAllReservationsOfSportsFacilityOfAddress(addressId, facilityId) {
    return await httpClient.get(`/addresses/${addressId}/sportsfacilities/${facilityId}/reservations`);
}

/**
 * general reservations pending and approved for /calendar view
 * all Roles
 */
export async function getCalendarReservationsOfSportsFacilityOfAddress(addressId, facilityId) {
    return await httpClient.get(`/addresses/${addressId}/sportsfacilities/${facilityId}/calendar`);
}

/**
 *  admin, trainer
 *  this starts the process -> notify supervisor -> supervisor gets the reservation from backend
 *  -> and puts it with changed status approved/denied
 */
export async function postReservationOfSportsFacilityOfAddress(addressId, facilityId, reservation) {
    return await httpClient.post(
        `/addresses/${addressId}/sportsfacilities/${facilityId}/reservations`,
        reservation
    );
}

/**
 *  admin
 *  supervisor can approve -> setting the status
 */
export async function putReservationStatusOfSportsFacilityOfAddress(addressId, facilityId, reservationId, status) {
    return await httpClient.put(
        `/addresses/${addressId}/sportsfacilities/${facilityId}/reservations/${reservationId}/status`,
        status,
        { headers: {'Content-Type': 'application/json'} }
    );
}

/**
 *  admin, trainer that owns reservation (isApplicant)
 */
export async function deleteReservationOfSportsFacilityOfAddress(addressId, facilityId, reservationId) {
    return await httpClient.delete(
        `/addresses/${addressId}/sportsfacilities/${facilityId}/reservations/${reservationId}`
    );
}

/**
 * admin
 */
export async function getAllUsers() {
    return await httpClient.get("/users");
}

/**
 * admin
 */
export async function postUser(user) {
    return await httpClient.post(`/users`, user);
}

/**
 * admin
 */
export async function putUser(user) {
    return await httpClient.put(`/users/${user.id}`, user);
}

/**
 * admin
 */
export async function deleteUser(userId) {
    return await httpClient.delete(`/users/${userId}`);
}
