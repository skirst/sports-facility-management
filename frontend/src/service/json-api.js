export const COURT_SURFACE = {
    GRASS: "GRASS",
    ARTIFICIAL_TURF: "ARTIFICIAL_TURF",
};

export const ROLE = {
    ADMIN: "ADMIN",
    CLUB_SUPERVISOR: "CLUB_SUPERVISOR",
    TRAINER: "TRAINER",
    PLAYER: "PLAYER",
};

export const RESERVATION_STATUS = {
    PENDING: "PENDING",
    APPROVED: "APPROVED",
    DENIED: "DENIED",
};

export const RESERVATION_PURPOSE = {
    GAME: "GAME",
    TRAINING: "TRAINING",
    RESERVED: "RESERVED",
    BLOCKED: "BLOCKED",
    MISC: "MISC",
};

export const RESERVATION_SPACE = {
    FULL_OF_FIELD: "FULL_FIELD",
    HALF_OF_FIELD: "HALF_OF_FIELD",
    THIRD_OF_FIELD: "THIRD_OF_FIELD"
};

/**
 * User: {id: number, name: string, email: string, telephoneNumber?: number, role: string}
 */
// eslint-disable-next-line
export const users = [
    {
        id: 0,
        name: "Admin",
        email: "admin@sports-facility.com",
        telephoneNumber: 999999999,
        role: ROLE.ADMIN,
    },
    {
        id: 1,
        name: "John Doe",
        email: "john.doe@sports-facility.com",
        telephoneNumber: 423456789,
        role: ROLE.CLUB_SUPERVISOR,
    },
    {
        id: 2,
        name: "Oliver Doe",
        email: "oliver.doe@sports-facility.com",
        telephoneNumber: 523456789,
        role: ROLE.TRAINER,
    },
    {
        id: 3,
        name: "Spieler 1",
        email: "spieler1@sports-facility.com",
        role: ROLE.PLAYER,
    },
];

/**
 * Reservation: {id: number, startTime: Date, endTime: Date, applicant: User, reservationStatus: string, reservationPurpose: string}
 */
// eslint-disable-next-line
export const reservations = [
    {
        id: 1,
        startTime: new Date(2022, 5, 12, 14, 30),
        endTime: new Date(2022, 5, 12, 17),
        applicant: users[2],
        reservationStatus: RESERVATION_STATUS.PENDING,
        reservationPurpose: RESERVATION_PURPOSE.GAME,
    },
];
/**
 * SportsFacility: {id: number, name: string, parallelReservations: number, floodlight: boolean, reservations: Reservation[], courtSurface: string, supervisors: User[]}
 */
// eslint-disable-next-line
export const sportsFacilities = [
    {
        id: 1,
        name: "SportsFacility1",
        parallelReservations: 2,
        floodlight: true,
        reservations: [reservations[0]],
        courtSurface: COURT_SURFACE.GRASS,
        supervisors: [users[1]],
    },
];

export const addresses = [
    {
        id: 1,
        street: "Taler-Allee",
        houseNumber: "3a",
        postalCode: 97070,
        location: "Würzburg",
    },
    {
        id: 1,
        street: "Frankenstraße",
        houseNumber: "15",
        postalCode: 97074,
        location: "Veitshöchheim",
    }
]