import {deleteUser, getAllUsers, postUser, putUser} from "@/service/http-service";
import {displayError} from "@/store/store-utils";
import {replaceOrInsertElWithId} from "@/utils";
import {ROLE} from "@/service/json-api";

const USER_MUTATION = {
    READ_USERS: "READ_USERS",
    CREATED_USER: "CREATED_USER",
    UPDATED_USER: "UPDATED_USER",
    REMOVED_USER: "REMOVED_USER",
    UPDATED_ACTIVE_USER: "UPDATED_ACTIVE_USER",
    CLEARED_USER_STATE: "CLEARED_USER_STATE"
};

export const USER_ACTION = {
    GET_USERS: "GET_USERS",
    POST_USER: "POST_USER",
    PUT_USER: "PUT_USER",
    DELETE_USER: "DELETE_USER",
    PUT_ACTIVE_USER: "PUT_ACTIVE_USER",
    CLEAR_USER_STATE: "CLEAR_USER_STATE"
};

const actions = {
    async [USER_ACTION.GET_USERS]({commit}) {
        try {
            const axiosResponse = await getAllUsers();
            commit(USER_MUTATION.READ_USERS, axiosResponse.data);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed fetching users!", error: axiosError});
        }
    },
    async [USER_ACTION.POST_USER]({commit}, user) {
        try {
            const axiosResponse = await postUser(user);
            commit(USER_MUTATION.CREATED_USER);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed creating user!", error: axiosError});
        }
    },
    async [USER_ACTION.PUT_USER]({commit}, user) {
        try {
            const axiosResponse = await putUser(user);
            commit(USER_MUTATION.UPDATED_USER);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed updating user!", error: axiosError});
        }
    },
    async [USER_ACTION.DELETE_USER]({commit}, userId) {
        try {
            await deleteUser(userId);
            commit(USER_MUTATION.REMOVED_USER);
            return {};
        } catch (axiosError) {
            displayError({msg: "Failed removing user!", error: axiosError});
        }
    },
    async [USER_ACTION.PUT_ACTIVE_USER]({commit}, user) {
        commit(USER_MUTATION.UPDATED_ACTIVE_USER, user);
    },
    async [USER_ACTION.CLEAR_USER_STATE]({commit}) {
        commit(USER_MUTATION.CLEARED_USER_STATE);
    },
};

const mutations = {
    [USER_MUTATION.READ_USERS](state, users) {
        state.users = users;
    },
    [USER_MUTATION.READ_USER](state, user) {
        replaceOrInsertElWithId(state.users, user);
    },
    [USER_MUTATION.CREATED_USER]() {
    },
    [USER_MUTATION.UPDATED_USER]() {
    },
    [USER_MUTATION.REMOVED_USER]() {
    },
    [USER_MUTATION.UPDATED_ACTIVE_USER](state, user) {
        state.activeUser = user;
    },
    [USER_MUTATION.CLEARED_USER_STATE](state) {
        state.activeUser = getActiveUserReset();
        state.users = [];
    },
};

const getters = {
    getUserNames(state) {
        return state.users.map(({id, username}) => `${id}: ${username}`);
    },
    getSupervisorUsers(state) {
        return state.users.filter((v) => v.role === ROLE.CLUB_SUPERVISOR);
    },
};

function getActiveUserReset() {
    return {
        id: -1,
        name: "",
        email: "",
        telephoneNumber: -1,
        role: "",
    }
}

// The state must return a function
// to make the module reusable.
// See: https://vuex.vuejs.org/en/modules.html#module-reuse
const state = () => ({
    users: [],
    activeUser: getActiveUserReset(),
});

export default {
    // We're using namespacing
    // in all of our modules.
    //namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
