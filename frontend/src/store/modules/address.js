import {deleteAddress, getAllAddresses, postAddress, putAddress} from "@/service/http-service";
import {displayError} from "@/store/store-utils";

const ADDRESS_MUTATION = {
    READ_ADDRESSES: "READ_ADDRESSES",
    CREATED_ADDRESS: "CREATED_ADDRESS",
    UPDATED_ADDRESS: "UPDATED_ADDRESS",
    REMOVED_ADDRESS: "REMOVED_ADDRESS",
    UPDATED_ACTIVE_ADDRESS: "UPDATED_ACTIVE_ADDRESS",
    CLEARED_ADDRESS_STATE: "CLEARED_ADDRESS_STATE",
};

export const ADDRESS_ACTION = {
    GET_ADDRESSES: "GET_ADDRESSES",
    POST_ADDRESS: "POST_ADDRESS",
    PUT_ADDRESS: "PUT_ADDRESS",
    DELETE_ADDRESS: "DELETE_ADDRESS",
    PUT_ACTIVE_ADDRESS: "PUT_ACTIVE_ADDRESS",
    CLEAR_ACTIVE_ADDRESS: "CLEAR_ACTIVE_ADDRESS",
    CLEAR_ADDRESS_STATE: "CLEAR_ADDRESS_STATE",
};

const actions = {
    async [ADDRESS_ACTION.GET_ADDRESSES]({commit}) {
        try {
            const axiosResponse = await getAllAddresses();
            commit(ADDRESS_MUTATION.READ_ADDRESSES, axiosResponse.data);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed fetching addresses!", error: axiosError});
        }
    },
    async [ADDRESS_ACTION.POST_ADDRESS]({commit}, address) {
        try {
            const axiosResponse = await postAddress(address);
            commit(ADDRESS_MUTATION.CREATED_ADDRESS);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed creating address!", error: axiosError});
        }
    },
    async [ADDRESS_ACTION.PUT_ADDRESS]({commit}, address) {
        try {
            const axiosResponse = await putAddress(address);
            commit(ADDRESS_MUTATION.UPDATED_ADDRESS);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed updating address!", error: axiosError});
        }
    },
    async [ADDRESS_ACTION.DELETE_ADDRESS]({commit}, addressId) {
        try {
            await deleteAddress(addressId);
            commit(ADDRESS_MUTATION.REMOVED_ADDRESS, addressId);
            return {};
        } catch (axiosError) {
            displayError({msg: "Failed removing address!", error: axiosError});
        }
    },
    async [ADDRESS_ACTION.PUT_ACTIVE_ADDRESS]({commit}, address) {
        commit(ADDRESS_MUTATION.UPDATED_ACTIVE_ADDRESS, address)
    },
    async [ADDRESS_ACTION.CLEAR_ACTIVE_ADDRESS]({commit}) {
        commit(ADDRESS_MUTATION.UPDATED_ACTIVE_ADDRESS, getResetForActiveAddress())
    },
    async [ADDRESS_ACTION.CLEAR_ADDRESS_STATE]({commit}) {
        commit(ADDRESS_MUTATION.CLEARED_ADDRESS_STATE)
    },
};

const mutations = {
    [ADDRESS_MUTATION.READ_ADDRESSES](state, addresses) {
        state.addresses = addresses;
    },
    [ADDRESS_MUTATION.CREATED_ADDRESS]() {
    },
    [ADDRESS_MUTATION.UPDATED_ADDRESS]() {
    },
    [ADDRESS_MUTATION.REMOVED_ADDRESS]() {
    },
    [ADDRESS_MUTATION.UPDATED_ACTIVE_ADDRESS](state, address) {
        state.activeAddress = address;
    },
    [ADDRESS_MUTATION.CLEARED_ADDRESS_STATE](state) {
        state.addresses = [];
        state.activeAddress = getResetForActiveAddress();
    },
};

const getters = {
    getAddressNames(state) {
        return state.addresses.map(({id, location, street, houseNumber}) =>
            `${id}: ${location + ":" + street + " " + houseNumber}`);
    },
    isActiveAddressSet: (state) => () => state.activeAddress.id !== -1,
};
function getResetForActiveAddress() {
     return {
        id: -1,
        street: "",
        houseNumber: "",
        postalCode: -1,
        location: "",
    }
}
// The state must return a function
// to make the module reusable.
// See: https://vuex.vuejs.org/en/modules.html#module-reuse
const state = () => ({
    addresses: [],
    activeAddress: getResetForActiveAddress()
});

export default {
    // We're using namespacing
    // in all of our modules.
    //namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
