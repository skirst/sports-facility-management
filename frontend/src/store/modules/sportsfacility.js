import {
    deleteSportsFacilityOfAddress,
    getAllSportsFacilitiesOfAddress,
    postSportsFacilityOfAddress,
    putSportsFacilityOfAddress
} from "@/service/http-service";
import {displayError} from "@/store/store-utils";

const FACILITY_MUTATION = {
    READ_FACILITIES: "READ_FACILITIES",
    CREATED_FACILITY: "CREATED_FACILITY",
    UPDATED_FACILITY: "UPDATED_FACILITY",
    REMOVED_FACILITY: "REMOVED_FACILITY",
    UPDATED_ACTIVE_FACILITY: "UPDATED_ACTIVE_FACILITY",
    CLEARED_FACILITY_STATE: "CLEARED_FACILITY_STATE",
};

export const FACILITY_ACTION = {
    GET_FACILITIES: "GET_FACILITIES",
    POST_FACILITY: "POST_FACILITY",
    PUT_FACILITY: "PUT_FACILITY",
    DELETE_FACILITY: "DELETE_FACILITY",
    PUT_ACTIVE_FACILITY: "PUT_ACTIVE_FACILITY",
    CLEAR_ACTIVE_FACILITY: "CLEAR_ACTIVE_FACILITY",
    CLEAR_FACILITY_STATE: "CLEAR_FACILITY_STATE",
};

const actions = {
    async [FACILITY_ACTION.GET_FACILITIES]({commit}, addressId) {
        try {
            const axiosResponse = await getAllSportsFacilitiesOfAddress(addressId);
            commit(FACILITY_MUTATION.READ_FACILITIES, axiosResponse.data);
            return axiosResponse.data
        } catch (axiosError) {
            displayError({msg: "Failed fetching sports facilities!", error: axiosError});
        }
    },
    async [FACILITY_ACTION.POST_FACILITY]({commit}, {addressId, facility}) {
        try {
            const axiosResponse = await postSportsFacilityOfAddress(addressId, facility);
            commit(FACILITY_MUTATION.CREATED_FACILITY);
            return axiosResponse.data
        } catch (axiosError) {
            displayError({msg: "Failed creating sports facility!", error: axiosError});
        }
    },
    async [FACILITY_ACTION.PUT_FACILITY]({commit}, {addressId, facility}) {
        try {
            const axiosResponse = await putSportsFacilityOfAddress(addressId, facility);
            commit(FACILITY_MUTATION.UPDATED_FACILITY);
            return axiosResponse.data
        } catch (axiosError) {
            displayError({msg: "Failed updating sports facility!", error: axiosError});
        }
    },
    async [FACILITY_ACTION.DELETE_FACILITY]({commit}, {addressId, facilityId}) {
        try {
            await deleteSportsFacilityOfAddress(addressId, facilityId);
            commit(FACILITY_MUTATION.REMOVED_FACILITY);
            return {};
        } catch (axiosError) {
            displayError({msg: "Failed deleting sports facility!", error: axiosError});
        }
    },
    async [FACILITY_ACTION.PUT_ACTIVE_FACILITY]({commit}, facility) {
        commit(FACILITY_MUTATION.UPDATED_ACTIVE_FACILITY, facility);
    },
    async [FACILITY_ACTION.CLEAR_ACTIVE_FACILITY]({commit}) {
        commit(FACILITY_MUTATION.UPDATED_ACTIVE_FACILITY, getResetForActiveFacility());
    },
    async [FACILITY_ACTION.CLEAR_FACILITY_STATE]({commit}) {
        commit(FACILITY_MUTATION.CLEARED_FACILITY_STATE);
    },
};

const mutations = {
    [FACILITY_MUTATION.READ_FACILITIES](state, facilities) {
        state.facilities = facilities;
    },
    [FACILITY_MUTATION.CREATED_FACILITY]() {
    },
    [FACILITY_MUTATION.UPDATED_FACILITY]() {
    },
    [FACILITY_MUTATION.REMOVED_FACILITY]() {
    },
    [FACILITY_MUTATION.UPDATED_ACTIVE_FACILITY](state, facility) {
        state.activeFacility = facility;
    },
    [FACILITY_MUTATION.CLEARED_FACILITY_STATE](state) {
        state.facilities = [];
        state.activeFacility = getResetForActiveFacility();
    },
};

const getters = {
    getSportsFacilitiesNames(state) {
        return state.facilities.map((fac) => `${fac.id}: ${fac.name}`);
    },
    isActiveFacilitySet: (state) => () => state.activeFacility.id !== -1,
};

function getResetForActiveFacility() {
    return {
        id: -1,
        name: "",
        parallelReservations: -1,
        floodlight: undefined,
        reservations: [],
        courtSurface: "",
        supervisors: [],
    };
}

// The state must return a function
// to make the module reusable.
// See: https://vuex.vuejs.org/en/modules.html#module-reuse
const state = () => ({
    facilities: [],
    activeFacility: getResetForActiveFacility(),
});

export default {
    // We're using namespacing
    // in all of our modules.
    //namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
