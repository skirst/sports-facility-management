import jwtDecode from "jwt-decode";
import router from "../../router"
import httpClient from "@/service/my-axios";
import {ROLE} from "@/service/json-api";
import {displayError} from "@/store/store-utils";

let inMemoryRefreshToken = undefined;

export const AUTH_ACTION = {
    POST_LOG_IN: "POST_LOG_IN",
    GET_TOKEN_REFRESH: "GET_TOKEN_REFRESH",
    CLEAR_AUTH_STATE: "CLEAR_AUTH_STATE"
};

const AUTH_MUTATION = {
    LOGGED_IN: "LOGGED_IN",
    REFRESHED_TOKEN: "REFRESHED_TOKEN",
    CLEARED_AUTH_STATE: "CLEARED_AUTH_STATE"
};

const actions = {
    async [AUTH_ACTION.POST_LOG_IN]({commit}, {username, password}) {
        try {
            delete httpClient.defaults.headers["Authorization"];
            let axiosResponse = await httpClient.post(`/login?username=${username}&password=${password}`);
            let authHeader = axiosResponse.headers["authorization"];
            commit(AUTH_MUTATION.LOGGED_IN, authHeader)
            return axiosResponse;
        } catch (axiosError) {
            displayError({msg: "Failed login! Check that your login data is correct!", error: axiosError})
        }
    },
    async [AUTH_ACTION.GET_TOKEN_REFRESH]({commit}) {
        try {
            let axiosResponse = await httpClient.get(`/auth/token-refresh`);
            let authHeader = axiosResponse.headers["authorization"];
            commit(AUTH_MUTATION.REFRESHED_TOKEN, authHeader)
            return axiosResponse;
        } catch (axiosError) {
            displayError({msg: "Failed re-authentication attempt!", error: axiosError})
        }
    },
    async [AUTH_ACTION.CLEAR_AUTH_STATE]({commit}) {
        inMemoryRefreshToken = undefined;
        delete httpClient.defaults.headers.common["Authorization"];
        commit(AUTH_MUTATION.CLEARED_AUTH_STATE)
    },
};

const mutations = {
    [AUTH_MUTATION.LOGGED_IN](state, jwt) {
        const decodedToken = decodeJwtSafe(jwt);
        state.role = decodedToken ? decodedToken.roles[0] : ""
        state.username = decodedToken ? decodedToken.sub : "";
    },
    [AUTH_MUTATION.REFRESHED_TOKEN](state, jwt) {
        const decodedToken = decodeJwtSafe(jwt);
        state.role = decodedToken ? decodedToken.roles[0] : ""
        state.username = decodedToken ? decodedToken.sub : "";
    },
    [AUTH_MUTATION.CLEARED_AUTH_STATE](state) {
        state.role = "";
        state.username = "";
    }
};

// The state must return a function
// to make the module reusable.
// See: https://vuex.vuejs.org/en/modules.html#module-reuse
const state = () => ({
    //the role of the logged in user
    role: "",
    //the userId of the logged in user
    username: ""
});

const getters = {
    role: (state) => () => state.role,
    isUserAdmin: (state) => () => state.role === ROLE.ADMIN,
    isUserSupervisor: (state) => () => state.role === ROLE.CLUB_SUPERVISOR,
    isUserTrainer: (state) => () => state.role === ROLE.TRAINER,
    isUserPlayer: (state) => () => state.role === ROLE.PLAYER,
    isUserReservationOwner: (state) => (username) => state.username === username,
};

export default {
    //namespaced: true,
    actions,
    mutations,
    getters,
    state,
};

/**
 * returns decoded body of jwt and undefined in case sth went wrong
 */
function decodeJwtSafe(encodedToken) {
    try {
        return jwtDecode(encodedToken);
    } catch {
        return undefined;
    }
}

function isRefreshTokenStillValid(decodedRefreshToken) {
    const refreshTokenDate = new Date();
    refreshTokenDate.setTime(decodedRefreshToken.exp * 1000);
    return refreshTokenDate.getTime() >= new Date().getTime();
}


export function storeApiPlugin (store) {
    httpClient.interceptors.response.use(
        (response) => {
            //if we get fresh tokens replace old ones
            if (response.headers["authorization"] && response.headers["refreshtoken"]) {
                //set default headers for all requests
                httpClient.defaults.headers.common["Authorization"] = `Bearer ${response.headers["authorization"]}`;
                inMemoryRefreshToken = response.headers["refreshtoken"];
            }
            return response;
        },
        async (error) => {
            //this config gets used by axios to execute a request from it
            const originalRequest = error?.config;

            //if the request was unauthorized -> token expired and we didn't retry yet
            if (error?.response?.status === 401 && !originalRequest?._didRetry) {
                const decodedRefreshToken = decodeJwtSafe(inMemoryRefreshToken);
                if (decodedRefreshToken !== undefined && isRefreshTokenStillValid(decodedRefreshToken)) {
                    try {
                        //set refresh Token as auth header for refresh request
                        httpClient.defaults.headers.common["Authorization"] = `Bearer ${inMemoryRefreshToken}`;
                        const refreshResponse = await store.dispatch(AUTH_ACTION.GET_TOKEN_REFRESH);
                        // we need to update the headers of old request otherwise it will fail again
                        originalRequest.headers["Authorization"] = `Bearer ${refreshResponse.headers["authorization"]}`;
                        //rerun unauthorized request
                        //_didRetry is custom and added by us to check that we only sent at most one retry request
                        originalRequest._didRetry = true;
                        //resend old request with modified auth header
                        return httpClient.request(originalRequest);
                    } catch {
                        //remove defaults, we want a new clean login
                        delete httpClient.defaults.headers.common["Authorization"];
                        inMemoryRefreshToken = undefined;

                        router.push({ name: 'login' })
                            .catch();
                        return Promise.reject(error);
                    }
                }
                //no valid refresh -> need to login
                delete httpClient.defaults.headers.common["Authorization"];
                inMemoryRefreshToken = undefined;
                router.push({ name: 'login' })
                    .catch();
                return Promise.reject(error);
            }
            //doing nothing with the response before it is returned
            return Promise.reject(error);
        }
    );

}