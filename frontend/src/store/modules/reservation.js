import {
    deleteReservationOfSportsFacilityOfAddress,
    getAllReservationsOfSportsFacilityOfAddress,
    getCalendarReservationsOfSportsFacilityOfAddress,
    postReservationOfSportsFacilityOfAddress,
    putReservationStatusOfSportsFacilityOfAddress
} from "@/service/http-service";
import {displayError} from "@/store/store-utils";
import {addTimezoneOffset} from "@/utils";

const RESERVATION_MUTATION = {
    READ_CALENDAR_RESERVATIONS: "READ_CALENDAR_RESERVATIONS",
    READ_RESERVATIONS: "READ_RESERVATIONS",
    CREATED_RESERVATION: "CREATED_RESERVATION",
    UPDATED_RESERVATION_STATUS: "UPDATED_RESERVATION_STATUS",
    REMOVED_RESERVATION: "REMOVED_RESERVATION",
    UPDATED_ACTIVE_RESERVATION: "UPDATED_ACTIVE_RESERVATION",
    CLEARED_RESERVATIONS: "CLEARED_RESERVATIONS",
    CLEARED_RESERVATION_STATE: "CLEARED_RESERVATION_STATE"
};

export const RESERVATION_ACTION = {
    GET_CALENDAR_RESERVATIONS: "GET_CALENDAR_RESERVATIONS",
    GET_RESERVATIONS: "GET_RESERVATIONS",
    POST_RESERVATION: "POST_RESERVATION",
    PUT_RESERVATION_STATUS: "PUT_RESERVATION_STATUS",
    DELETE_RESERVATION: "DELETE_RESERVATION",
    PUT_ACTIVE_RESERVATION: "PUT_ACTIVE_RESERVATION",
    CLEAR_ACTIVE_RESERVATION: "CLEAR_ACTIVE_RESERVATION",
    CLEAR_RESERVATIONS: "CLEAR_RESERVATIONS",
    CLEAR_RESERVATION_STATE: "CLEAR_RESERVATION_STATE",
};

const actions = {
    async [RESERVATION_ACTION.GET_CALENDAR_RESERVATIONS]({commit}, {addressId, facilityId}) {
        try {
            const axiosResponse = await getCalendarReservationsOfSportsFacilityOfAddress(addressId, facilityId);
            commit(RESERVATION_MUTATION.READ_CALENDAR_RESERVATIONS, axiosResponse.data);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed fetching public calendar reservations!", error: axiosError});
        }
    },
    async [RESERVATION_ACTION.GET_RESERVATIONS]({commit}, {addressId, facilityId}) {
        //personal role based request
        try {
            const axiosResponse = await getAllReservationsOfSportsFacilityOfAddress(addressId, facilityId);
            commit(RESERVATION_MUTATION.READ_RESERVATIONS, axiosResponse.data);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed fetching sports reservations!", error: axiosError});
        }
    },
    async [RESERVATION_ACTION.POST_RESERVATION]({commit}, {addressId, facilityId, reservation}) {
        try {
            const axiosResponse = await postReservationOfSportsFacilityOfAddress(addressId, facilityId, reservation);
            commit(RESERVATION_MUTATION.CREATED_RESERVATION);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed creating sports reservation!", error: axiosError});
        }
    },
    async [RESERVATION_ACTION.PUT_RESERVATION_STATUS]({commit}, {addressId, facilityId, reservationId, status}) {
        try {
            const axiosResponse = await putReservationStatusOfSportsFacilityOfAddress(addressId, facilityId, reservationId, status);
            commit(RESERVATION_MUTATION.UPDATED_RESERVATION_STATUS);
            return axiosResponse.data;
        } catch (axiosError) {
            displayError({msg: "Failed updating sports reservation!", error: axiosError});
        }
    },
    async [RESERVATION_ACTION.DELETE_RESERVATION]({commit}, {addressId, facilityId, reservationId}) {
        try {
            await deleteReservationOfSportsFacilityOfAddress(addressId, facilityId, reservationId);
            commit(RESERVATION_MUTATION.REMOVED_RESERVATION);
            return {};
        } catch (axiosError) {
            displayError({msg: "Failed deleting reservation!", error: axiosError});
        }
    },
    async [RESERVATION_ACTION.PUT_ACTIVE_RESERVATION]({commit}, reservation) {
        commit(RESERVATION_MUTATION.UPDATED_ACTIVE_RESERVATION, reservation)
    },
    async [RESERVATION_ACTION.CLEAR_ACTIVE_RESERVATION]({commit}) {
        commit(RESERVATION_MUTATION.UPDATED_ACTIVE_RESERVATION, getResetForActiveReservation());
    },
    async [RESERVATION_ACTION.CLEAR_RESERVATIONS]({commit}) {
        commit(RESERVATION_MUTATION.CLEARED_RESERVATIONS);
    },
    async [RESERVATION_ACTION.CLEAR_RESERVATION_STATE]({commit}) {
        commit(RESERVATION_MUTATION.CLEARED_RESERVATION_STATE);
    },
};

const mutations = {
    [RESERVATION_MUTATION.READ_CALENDAR_RESERVATIONS](state, reservations) {
        state.reservations = reservations;
    },
    [RESERVATION_MUTATION.READ_RESERVATIONS](state, reservations) {
        state.reservations = reservations;
    },
    [RESERVATION_MUTATION.CREATED_RESERVATION]() {
    },
    [RESERVATION_MUTATION.UPDATED_RESERVATION_STATUS]() {
    },
    [RESERVATION_MUTATION.REMOVED_RESERVATION]() {
    },
    [RESERVATION_MUTATION.UPDATED_ACTIVE_RESERVATION](state, reservation) {
        state.activeReservation = reservation;
    },
    [RESERVATION_MUTATION.CLEARED_RESERVATIONS](state) {
        state.reservations = [];
    },
    [RESERVATION_MUTATION.CLEARED_RESERVATION_STATE](state) {
        state.reservations = [];
        state.activeReservation = getResetForActiveReservation();
    },
};

const getters = {
    getReservationNames: (state) => {
        return state.reservations.map(({id, startTime}) =>
            `${id}: ${startTime}`);
    },
    isActiveReservationSet: (state) => state.activeReservation.id !== -1,
    reservationsTimeZoneOffsetCalc: (state) => {
        return state.reservations.map((v) => {
            v.startTime = addTimezoneOffset(v.startTime);
            v.endTime = addTimezoneOffset(v.endTime);
            return v})
        },
};

function getResetForActiveReservation() {
    return {
        id: -1,
        startTime: new Date(1970, 1, 1, 0, 0),
        endTime: new Date(1970, 1, 1, 0, 0),
        applicant: {},
        reservationStatus: "",
        reservationPurpose: "",
    }
}

// The state must return a function
// to make the module reusable.
// See: https://vuex.vuejs.org/en/modules.html#module-reuse
const state = () => ({
    reservations: [],
    activeReservation: getResetForActiveReservation(),
});

export default {
    // We're using namespacing
    // in all of our modules.
    //namespaced: true,
    actions,
    mutations,
    getters,
    state,
};
