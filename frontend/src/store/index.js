import Vue from 'vue'
import Vuex from 'vuex'
import Address, {ADDRESS_ACTION} from "./modules/address";
import Sportsfacility, {FACILITY_ACTION} from "./modules/sportsfacility";
import Reservation, {RESERVATION_ACTION} from "./modules/reservation";
import User, {USER_ACTION} from "./modules/user";
import Auth, {AUTH_ACTION, storeApiPlugin} from "./modules/auth";

Vue.use(Vuex)

export const CALENDAR_MUTATION = {
    UPDATED_CALENDARVIEW:'UPDATED_CALENDARVIEW',
    UPDATED_DAY:'UPDATED_DAY',
}

export const CALENDAR_ACTION = {
    PUT_CALENDARVIEW:'PUT_CALENDARVIEW',
    PUT_DAY:'PUT_DAY',
}

export const STORE_MUTATION = {
    CLEARED_STORE: "CLEARED_STORE"
}

export const STORE_ACTION = {
    CLEAR_STORE: "CLEAR_STORE"
}

export default new Vuex.Store({
    state: {
        activeCalendarView: 'month',
        activeDate: '',
    },
    getters: {},
    mutations: {
        [CALENDAR_MUTATION.UPDATED_CALENDARVIEW](state, choosenCalendarView) {
            state.activeCalendarView = choosenCalendarView;
        },
        [CALENDAR_MUTATION.UPDATED_DAY](state, choosenDay) {
            state.activeDate = choosenDay;
        },
        [STORE_MUTATION.CLEARED_STORE](state) {
            state.activeCalendarView = 'month';
            state.activeDate = '';
        },
    },
    actions: {
        async [CALENDAR_ACTION.PUT_CALENDARVIEW]({commit}, choosenCalendarView) {
            commit(CALENDAR_MUTATION.UPDATED_CALENDARVIEW, choosenCalendarView);
        },
        async [CALENDAR_ACTION.PUT_DAY]({commit}, choosenDate) {
            commit(CALENDAR_MUTATION.UPDATED_DAY, choosenDate);
        },
        async [STORE_ACTION.CLEAR_STORE]({commit, dispatch}) {
            await dispatch(ADDRESS_ACTION.CLEAR_ADDRESS_STATE);
            await dispatch(FACILITY_ACTION.CLEAR_FACILITY_STATE);
            await dispatch(RESERVATION_ACTION.CLEAR_RESERVATION_STATE);
            await dispatch(USER_ACTION.CLEAR_USER_STATE);
            await dispatch(AUTH_ACTION.CLEAR_AUTH_STATE);
            commit(STORE_MUTATION.CLEARED_STORE);
        },
    },
    modules: {
        address: Address,
        sportsfacility: Sportsfacility,
        reservation: Reservation,
        user: User,
        auth: Auth
    },
    plugins: [
        storeApiPlugin
    ]
})
