import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import i18n from '@/plugins/i18n';
import FlagIcon from 'vue-flag-icon';
import Vuelidate from 'vuelidate';
import {ADDRESS_ACTION} from "@/store/modules/address";
import {FACILITY_ACTION} from "@/store/modules/sportsfacility";
import {RESERVATION_ACTION} from "@/store/modules/reservation";
import {USER_ACTION} from "@/store/modules/user";

Vue.use(FlagIcon, Vuelidate);
Vue.config.productionTip = false;

router.beforeEach( (to, from, next) => {
  //we need to call next to resolve the hook
  //also next may only be called once (else undefined behavior or errors may happen),
  // this can be achieved by either if-else structures or explicit returns
  const userRole = store.getters.role();
  //only login is allowed to be accessed without authorization
  if (to.name !== "login" && userRole === "") {
    //reroute to login
    next({ name: "login" });
    return; //returning to prevent further calls to next
  }
  //if User wants to access a route he isn't authorized deny it
  if (!isUserAuthorizedFor(to, userRole)) {
    next(false);
    return; //returning to prevent further calls to next
  }

  next()
})

// eslint-disable-next-line no-unused-vars
function isUserAuthorizedFor(toRoute, userRole) {
  const {requiresAuth, allowedRoles} = toRoute.meta;
  //route requires Authority and a specific role
  return requiresAuth
      ? allowedRoles.some((v) => v === userRole)
      : true;
}

function shouldClear(to, from) {
  //exception to clear: when we go to reservationEdit we want to keep all prior data, also when returning from it to calendar
  if( (from.path==="/calendar" && to.path==="/reservationEdit")
      || (from.path==="/reservationEdit" && to.path==="/calendar") )
  { return false }
  else {
    return from.meta.clearOnLeave;
  }
}

//TODO: use for auth checking and authorization https://router.vuejs.org/guide/advanced/meta.html
 router.afterEach(async (to, from) => {
  if(shouldClear(to, from)) {
    //clear state to inforce always refetching of fresh data
    await store.dispatch(ADDRESS_ACTION.CLEAR_ADDRESS_STATE);
    await store.dispatch(FACILITY_ACTION.CLEAR_FACILITY_STATE);
    await store.dispatch(RESERVATION_ACTION.CLEAR_RESERVATION_STATE);
    await store.dispatch(USER_ACTION.CLEAR_USER_STATE);
  }
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app');
