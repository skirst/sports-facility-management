import Vue from 'vue'
import VueRouter from 'vue-router'
import CalendarView from "@/views/CalendarView.vue"
import UserEditView from "@/views/UserEditView.vue"
import FacilityEditView from "@/views/FacilityEditView.vue"
import GroundEditView from "@/views/GroundEditView.vue"
import ReservationEditView from "@/views/ReservationEditView.vue"
import {ROLE} from "@/service/json-api";
import LoginForm from "@/components/LoginForm";

Vue.use(VueRouter)

const allRolesAllowed = Object.values(ROLE);

const routes = [
  {
    path: "/about",
    name: "about",
    meta: {requiresAuth: true, clearOnLeave: false, allowedRoles: allRolesAllowed},
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/AboutView.vue"),
  },
  {
    path: "/login",
    name: "login",
    meta: {requiresAuth: false, clearOnLeave: false, allowedRoles: allRolesAllowed},

    component:LoginForm,
  },
  {
    path: "/calendar",
    name: "calendar",
    meta: {requiresAuth: true, clearOnLeave: true, allowedRoles: allRolesAllowed},

    component: CalendarView,
  },
  {
    path: "/userEdit",
    name: "userEdit",
    meta: {requiresAuth: true, clearOnLeave: true, allowedRoles: [ROLE.ADMIN]},

    component: UserEditView,
  },
  {
    path: "/facEdit",
    name: "facEdit",
    meta: {requiresAuth: true, clearOnLeave: true, allowedRoles: [ROLE.ADMIN]},

    component: FacilityEditView,
  },
  {
    path: "/groundEdit",
    name: "groundEdit",
    meta: {requiresAuth: true, clearOnLeave: true, allowedRoles: [ROLE.ADMIN]},

    component: GroundEditView,
  },
  {
    path: "/reservationEdit",
    name: "reservationEdit",
    meta: {requiresAuth: true, clearOnLeave: false, allowedRoles: [ROLE.ADMIN, ROLE.CLUB_SUPERVISOR, ROLE.TRAINER]},

    component: ReservationEditView,
  },
  {
    path: "/fhws",
    name: "fhws",
    meta: {requiresAuth: false, clearOnLeave: false, allowedRoles: allRolesAllowed},

    //re-routing to external website
    beforeEnter() {
      window.location.href = "https://fiw.fhws.de";
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
