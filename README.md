# sportstaettenverwaltung
WebApp zur Verwaltung von Belegung von Sportstätten

Bauen und Starten der Anwendung mittels Docker Compose im Projektordner:
```
cd ./docker
docker-compose up
```

Testdaten (User mit verschiedenen Rollen/Berechtigungen für Login):

* **ADMIN:** tommy - a_secret_pw
* **PLAYER:** sanny - another_secret_pw
* **TRAINER:** kalli - my-password
* **SUPERVISOR:** richie - my-pw
